<%
'######################################################################
'## ab.d.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Common Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/01/12 12:57
'## Description :   AspBox一般函数(不常用)类
'######################################################################

Class Cls_AB_D

	Private o_tmp

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.IsBlank(s) 判断字符串值是否为空
	'@ 返  回:  True/False [Boolean]
	'@ 作  用:  判断字符串值是否为空, 免去了判断字段类型的麻烦
	'@ Author:  Lajox; version: 1.0.0 (2011-05-29);
	'==DESC=====================================================================================
	'@ 参数 s : 待检测的字符串
	'@ =====
	'@ VarType返回值如下:
	'@ vbEmpty 0 Empty（未初始化）
	'@ vbNull 1 Null（无有效数据）
	'@ vbInteger 2 整数
	'@ vbLong 3 长整数
	'@ vbSingle 4 单精度浮点数
	'@ vbDouble 5 双精度浮点数
	'@ vbCurrency 6 货币
	'@ vbDate 7 日期
	'@ vbString 8 字符串
	'@ vbObject 9 Automation 对象
	'@ vbError 10 错误
	'@ vbBoolean 11 Boolean
	'@ vbVariant 12 Variant（只和变量数组一起使用）
	'@ vbDataObject 13 数据访问对象
	'@ vbByte 17 字节
	'@ vbArray 8192 数组
	'==DEMO=====================================================================================
	'@ AB.D.IsBlank("abc") => False
	'@ *****************************************************************************************

	Function IsBlank(ByRef s)
		IsBlank = False
		' Select Case True
			' Case IsObject(s)
				' If s Is Nothing Then IsBlank = True : Exit Function
			' Case IsEmpty(s), IsNull(s)
				' IsBlank = True : Exit Function
			' Case IsArray(s)
				' If UBound(s) = 0 Then IsBlank = True : Exit Function
			' Case IsNumeric(s)
				' If (s = 0) Then IsBlank = True : Exit Function
			' Case Else
				' If Trim(s) = "" Then IsBlank = True : Exit Function
		' End Select
		Select Case VarType(s)
			Case 0,1 '类型: Empty & Null
				IsBlank = True
			Case 8 '类型: String
				If Len(s) = 0 Then
				IsBlank = True
				End If
			Case 9 '类型: Object
				tmpType = TypeName(s)
				If (tmpType = "Nothing") Or (tmpType = "Empty") Then
				IsBlank = True
				End If
			Case 8192, 8204, 8209 '类型: Array
				'does it have at least one element?
				If UBound(s) = -1 Then
				IsBlank = True
			End If
		End Select
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.CNum(s) 转化字符串为整型
	'@ 返  回:  [Integer]
	'@ 作  用:  强制转化字符串为整型
	'==DESC=====================================================================================
	'@ 参数 s : 原字符串
	'==DEMO=====================================================================================
	'@ AB.D.CNum("1001") => 1001
	'@ *****************************************************************************************

	Function CNum(Byval s)
		On Error Resume Next
		Dim oInt:oInt=s
		IF Cstr(oInt)="0" Then:CNum=0:Exit Function:End IF
		IF CheckExp(oInt,"^[\-]?\d+$")=True Then
			CNum=CLng(oInt)
		Else
			CNum=CLng(oInt):Err.Clear
		End IF
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.GetV(s)
	'@ 返  回:  原字符串 Or ""
	'@ 作  用:  如果字符串为Null则返回空值(""),否则返回原字符串
	'==DESC=====================================================================================
	'@ 参数 s : 字符串
	'==DEMO=====================================================================================
	'@ 无
	'@ *****************************************************************************************

	Function GetV(Byval s)
		Dim v:v=s
		If IsNull(v) or v="" Then
			GetV=""
		Else
			GetV=v
		End if
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.CheckExp(s, rule)
	'@ 返  回:  True/False
	'@ 作  用:  验证是否满足正则表达式
	'==DESC=====================================================================================
	'@ 参数 s:		[String] 待检测的字符串
	'@ 参数 rule:	[String] 正则表达式规则
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.D.CheckExp("abcd", "a(bc)?d") => True
	'@ *****************************************************************************************

	Function CheckExp(Byval s, Byval rule)
		Dim RegEx, Match
		Set RegEx = New RegExp
		RegEx.Pattern = rule
		'RegEx.MultiLine = True '多行属性
		RegEx.IgnoreCase = True '设置是否区分字符大小写
		RegEx.Global = True '设置全局可用性
		Match = RegEx.Test(s) '执行搜索
		CheckExp = Match
		Set RegEx = Nothing
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.RegReplace(str, patrn, rpstr) 正则替换函数
	'@ 返  回:  [String]
	'@ 作  用:  用正则表达式替换字符串
	'==DESC=====================================================================================
	'@ 参数 str:	[String] 原字符串
	'@ 参数 patrn:	[String] 正则表达式规则
	'@ 参数 rpstr:	[String] 替换字符串
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.D.RegReplace("I<like>Asp", "<.*>", "") => IAsp
	'@ *****************************************************************************************

	Function RegReplace(Byval str, Byval patrn, Byval rpstr)
		On Error Resume Next
		Dim RegEx, RtStr
		Set RegEx = New RegExp '建立正则表达式
		RegEx.Pattern = patrn '设置模式
		RegEx.IgnoreCase = true '设置是否区分字符大小写
		RegEx.Global = True '设置全局可用性
		RtStr = RegEx.Replace( Cstr(str), Cstr(rpstr))
		RegReplace = RtStr
		Set RegEx = Nothing
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.ArrReplace(s, a1, a2) 按数组形式对应替换某字符串
	'@ 返  回:  [String]
	'@ 作  用:  在长字符串中查找数组1各元素的字符串替换成数组2各元素的字符串
	'==DESC=====================================================================================
	'@ 参数 s:	[String] 原字符串
	'@ 参数 a1:	[Array] 数组1
	'@ 参数 a2:	[Array] 数组2
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.D.ArrReplace("abcdefg", Array("b","c"), Array("x","y")) => axydefg
	'@ *****************************************************************************************

	Function ArrReplace(Byval s,Byval a1,Byval a2)
		IF Err Then Err.Clear
		On Error Resume Next
		Dim temp:temp=s
		Dim I:I=0
		IF IsArray(a1) And IsArray(a2) Then 'arr1和arr2都是数组
			IF UBound(a1)<=UBound(a2) Then '当数组arr1不大于arr2数组时，对应匹配替换
				For I=0 To UBound(a1)
					temp = AB.C.RP(temp, a1(I), a2(I))
				Next
			Else '当数组arr1大于arr2数组时，前面部分对应匹配，arr1超过arr2的部分替换为空
				For I=0 To UBound(a2)
					temp = AB.C.RP(temp, a1(I), a2(I))
				Next
				For I=UBound(a2)+1 To UBound(a1)
					temp = AB.C.RP(temp, a1(I), "")
				Next
			End IF
		ElseIF IsArray(a1) And (Not IsArray(a2)) Then 'arr1是数组，arr2不是数组
			For I=0 To UBound(a1)
				temp = AB.C.RP(temp, a1(I), a2)
			Next
		ElseIF (Not IsArray(a1)) And (Not IsArray(a2)) Then 'arr1和arr2都不是数组
			temp = AB.C.RP(temp, a1, a2)
		Else '其他情况（arr1不是数组，arr2是数组）
			IF IsArray(a2) Then temp = AB.C.RP(temp, a1, a2(0))
		End IF
		ArrReplace = temp
		On Error Goto 0
	End Function

	'@ ********************************************************************************
	'@ AB.D.ReplaceAll(sString, sFind, sReplaceWith, bAll) 全部替换字符串，
	'@ 功能: 其中指定数目的某子字符串 全部 被替换为另一个子字符串。
	'==DESC============================================================================
	'@ sString : 		某字符串
	'@ sFind : 			查找 “要替换的子字符串”
	'@ sReplaceWith : 	替换 “替换后的子字符串”
	'@ bAll : 			是否忽略大小写, True表忽略大小写, False表区分大小写
	'==DEMO============================================================================
	'@  AB.D.ReplaceAll("hijklmnopqrst HIJKLMNOPQRST lMn", "lmn", "---", False) 区分大小写
	'@  AB.D.ReplaceAll("hijklmnopqrst HIJKLMNOPQRST lMn", "lmn", "---", True) 忽略大小写
	'@ ********************************************************************************

	Function ReplaceAll(Byval sString, Byval sFind, Byval sReplaceWith, Byval bAll)
		Dim str : str = sString
		If IsNull(str) Then ReplaceAll = "" : Exit Function
		If CBool(bAll) Then
			Do While InStr( 1, str, sFind, 1) > 0
				str = Replace(str, sFind, sReplaceWith, 1, -1, 1)
				If InStr( 1, sReplaceWith , sFind , 1) >0 Then Exit Do
			Loop
		Else
			Do While InStr(str, sFind) > 0
				str = Replace(str, sFind, sReplaceWith)
				If InStr(sReplaceWith, sFind ) > 0 Then Exit Do
			Loop
		End If
		ReplaceAll = str
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.CheckErr(s) 检测脚本是否出错(s为执行脚本)
	'@ 返  回:  True/False
	'@ 作  用:  检测脚本是否出错,如果出错返回True,不出错返回False
	'==DESC=====================================================================================
	'@ 参数 s:	[String] 执行脚本字符串
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.D.CheckErr("Set Obj = Nothing") => False
	'@ *****************************************************************************************

	Function CheckErr(Byval s)
		CheckErr = False
		On Error Resume Next
		Err.Clear
		Dim exeScript
		'exeScript = Eval(s)
		exeScript = Execute(s)
		IF Err Then
			CheckErr = True
			Err.Clear
		End IF
		On Error Goto 0
	End Function

	'---------------------------------------------------------------------
	'# AB.D.FillNum(num,n,pos)
	'# @return: string
	'# @dowhat: 补位数,返回n位数字如0005
	'# @author: Lajox; version: 1.0.0 (2011-05-28)
	'--DESC---------------------------------------------------------------
	'# @param num [numeric] : 要转化的数字
	'# @param n [integer] : 要转化后的数字位数
	'# @param pos [integer] : 在前面或后面补0位置, 0为前面补0, 1为后面补0
	'--DEMO---------------------------------------------------------------
	'# AB.C.Print AB.D.FillNum("5",2,0) '输出：05
	'#--------------------------------------------------------------------

	Function FillNum(num,n,pos)
		if not IsNumeric(num) then
			FillNum=num
		else
			if pos=0 then '
				if len(cstr(num))>=n then
					FillNum=num
				else
					FillNum="0"&cstr(num)
					while len(FillNum)<n
						FillNum="0"&FillNum
					wend
				end if
			else
				if len(cstr(num))>=n then
					FillNum=num
				else
					FillNum=cstr(num)&"0"
					while len(FillNum)<n
						FillNum=FillNum&"0"
					wend
				end if
			end if
		end if
	End Function

	Public Function SqlInCheck(Key) '安全检查(检查是否含有非法字符)
		On Error Resume Next
		Dim ParaValue,CheckValue
		CheckValue=True
		ParaValue=Trim(Lcase(Key))
		IF IsNumeric(ParaValue) Then
			SqlInCheck=True
			Exit Function
		Else
			Dim SqlStr,SqlArray,i
				SqlStr="%27|'|''|;|*|and|select|update|delete|count|exec|dbcc|alter|drop|insert|master|truncate|char|declare|where|set|declare|mid|chr"
			IF AB.C.IsNul(ParaValue) Then:SqlInCheck=True:Exit Function:End IF
			SqlArray=Split(SqlStr,"|")
			For i=0 To Ubound(SqlArray)
				IF Instr(ParaValue,SqlArray(i))>0 Then
					CheckValue=False
					Exit For
				End IF
			Next
			SqlInCheck=CheckValue
		End IF
		On Error Goto 0
	End Function

	Public Function SqlInFilter(ByVal QueryStr)
		On Error Resume Next
		Dim SqlStr,SqlArray,i
			SqlStr="%27|'|''|;|*|and|select|update|delete|count|exec|dbcc|alter|drop|insert|master|truncate|char|declare|where|set|declare|mid|chr"
		IF AB.C.IsNul(QueryStr) Then:SqlInFilter="":Exit Function:End IF
		SqlArray=Split(SqlStr,"|")
		For i=0 To Ubound(SqlArray)
			IF Instr(Lcase(QueryStr),SqlArray(i)) > 0 Then
				'QueryStr=Replace(Lcase(QueryStr),SqlArray(i),"")
				'QueryStr=RegReplace(QueryStr, SqlArray(i), "")
				QueryStr=Replace(QueryStr,SqlArray(i),"",1,-1,1)
			End IF
		next
		SqlInFilter =Server.HTMLEncode(QueryStr)
		On Error Goto 0
	End Function

	Function SafeRequest(Key,Modes)
		Dim ParaValue
		Select Case Lcase(Modes)
			Case "get"
				ParaValue=Trim(Request.QueryString(Key))
			Case "post"
				ParaValue=Trim(Request.Form(Key))
			Case "auto"
				ParaValue=Trim(Request(Key))
		End Select
		IF IsNumeric(ParaValue)  Then
			SafeRequest=ParaValue
			Exit Function
		Else
			IF Cstr(ParaValue)="0" Then:SafeRequest=0:Exit Function:End IF
			IF AB.C.Test(ParaValue, "^[\-]?\d+$")=True Then
				SafeRequest=CLng(ParaValue)
			Else
				SafeRequest=SqlInFilter(ParaValue)
				IF Err Then Err.Clear
			End IF
		End IF
	End Function

	Function Safe(Key) '安全过滤字符
		IF Err Then Err.Clear
		On Error Resume Next
		Dim ParaValue:ParaValue=Trim(Key)
		IF AB.C.IsNul(ParaValue) Then:Safe="":Exit Function:End IF
		IF IsNumeric(ParaValue) Then
			Safe=ParaValue:Exit Function
		Else
			IF Cstr(ParaValue)="0" Then:Safe=0:Exit Function:End IF
			IF AB.C.Test(ParaValue, "^[\-]?\d+$")=True Then
				Safe=CLng(ParaValue)
			Else
				Safe=SqlInFilter(ParaValue):Err.Clear
			End IF
		End IF
		On Error Goto 0
	End Function

	Function Cip(Byval sip) 'e.g response.write Cip("110.84.25.148")
		Dim tip:tip = CStr(sip)
		Dim sip1,sip2,sip3,sip4
		sip1 = Left(tip, CInt(InStr(tip, ".") -1))
		tip = Mid(tip, CInt(InStr(tip, ".") + 1))
		sip2 = Left(tip, CInt(InStr(tip, ".") -1))
		tip = Mid(tip, CInt(InStr(tip, ".") + 1))
		sip3 = Left(tip, CInt(InStr(tip, ".") -1))
		sip4 = Mid(tip, CInt(InStr(tip, ".") + 1))
		Cip = CInt(sip1) * 256 * 256 * 256 + CInt(sip2) * 256 * 256 + CInt(sip3) * 256 + CInt(sip4)
	End Function

	'当值为空，返回设定值
    Public Function NullIs(Byval sValue,Byval oStr)
        Dim RtStr:RtStr=oStr
        IF IsNull(oStr) or oStr="" Then RtStr=""
        IF IsNull(sValue) Or sValue="" Then
            sValue = RtStr
        End IF
        NullIs = sValue
    End Function

	'删除多余的(连续的)空格(No extra space; remove Superfluous Space)
	Function NsSpace(str)
		 Dim re
		 Set re=New RegExp
		 re.IgnoreCase =True
		 re.Global=True
		 re.Pattern="(\s{1,})"
		 str=re.replace(str," ")
		 NsSpace=str
		 Set re=Nothing
	End Function

	'删除和处理空格
	Function DelSpace(Byval s, Byval p)
		Dim temp:temp=s
		Dim n:n=0
		If IsNumeric(p) Then n=Cint(p)
		Select Case n
			Case 0: '清除首尾空格
				temp = Trim(temp)
			Case 1: '删除全部空格
				temp = AB.C.RegReplace(temp, "\s", "")
			Case 2: '去除全部空行
				temp = AB.C.RegReplace(temp, "\n[\s| ]*\r?", "")
				temp = AB.C.RegReplace(temp, "^[\s\n]*([\s\S]*)$", "$1")
				temp = AB.C.RegReplace(temp, "([\S]+?)[\s]*$", "$1")
			Case 3: '去除以空行及空格开头字符
				temp = AB.C.RegReplace(temp, "^[\s\n]*([\s\S]*)$", "$1")
			Case 4: '去除以空行及空格结尾字符
				temp = AB.C.RegReplace(temp, "([\S]+?)[\s]*$", "$1")
			Case 5: '删除每行当中的空格开头字符(且删除空行)
				temp = AB.C.RegReplaceM(temp, "^\s*(.*)\s*$", "$1")
				temp = AB.C.RegReplace(temp, "^[\s\n]*([\s\S]*)$", "$1")
				temp = AB.C.RegReplace(temp, "([\S]+?)[\s]*$", "$1")
			Case 6: '去除空格结尾字符(如果是空行则删除空行)
				temp = AB.C.RegReplaceM(temp, "(\S+)\s*$", "$1")
				temp = AB.C.RegReplace(temp, "^[\s\n]*([\s\S]*)$", "$1")
				temp = AB.C.RegReplace(temp, "([\S]+?)[\s]*$", "$1")
			Case Else:
				temp = temp
		End Select
		DelSpace = temp
	End Function

	'删除每行从某字符开始的后面字符
	Function DelBehind(Byval s, Byval c, Byval p)
		Dim temp:temp=s
		Dim n:n=0
		If IsNumeric(p) Then n=Cint(p)
		Select Case n
			Case 0: '删除每行的从某字符开始之后的以外所有字符(除了换行符)
				temp = AB.C.RegReplaceM(temp,""& c &"[^\n\r]*", "")
			Case 1: '删除每行的从某字符开始之后的所有字符(包括换行符)
				temp = AB.C.RegReplace(temp, "\s*?"& c &"[^\n\r]*", "")
			Case 2: '删除每行的从某字符开始之后的所有字符(除了换行符)(此方法能适应Notepad++编辑器正则规则)
				temp = AB.C.RegReplace(temp, "([\s\n]*[\s\S]*)" & c &"[^\n\r]*", "$1")
		End Select
		DelBehind = temp
	End Function

	'执行ASP脚本
	Function Exec(Byval oStr)
		Dim tStr:tStr=oStr
		IF AB.C.IsNul(tStr) Then: tStr="": Exit Function: End IF
		On Error Resume Next
		Execute tStr
		On Error Goto 0
	End Function

	'将英文引号转化为中文引号
	Function cquot(Byval Str)
		IF AB.C.IsNul(Str) Then : cquot=Str : Exit Function
		Str=Replace(Str,"""","”") '英文单引号引号" 转 中文单引号”
		Str=Replace(Str,"'","’") '英文单引号引号' 转 中文单引号’
		cquot=Str
	End Function

	'删除过滤英文引号
	Function dquot(Byval Str)
		IF AB.C.IsNul(Str) Then : dquot=Str : Exit Function
		Str=Replace(Str,"""","") '删除过滤英文单引号引号"
		Str=Replace(Str,"'","") '删除过滤英文单引号引号'
		dquot=Str
	End Function

	'将英文引号格式化(为\"或\')
	Function fquot(Byval Str)
		IF AB.C.IsNul(Str) Then : fquot=Str : Exit Function
		Str=Replace(Str,"""","\""") '英文单引号引号" 格式化为 \"
		Str=Replace(Str,"'","\'") '英文单引号引号' 格式化为 \'
		fquot=Str
	End Function

	'动态改变变量名值
	Function SetValue(ByVal TName,ByVal TValue)
		IF Err Then Err.Clear
		On Error Resume Next
		IsEmptyName=False
		Dim tStr:tStr=Eval(""&TName&"")
		IF Err Then:IsEmptyName=True:Err.Clear:End IF
		IF Not IsEmptyName Then
			IF Not IsNumeric(TValue) Then
				Execute ""&TName&"="""&TValue&""""
			Else
				Execute ""&TName&"="&TValue&""
			End IF
		End IF
		On Error Goto 0
	End Function

	'提取SQL字段值; 示例: AB.D.GetRs("select top 1 id,name from LB_X_Admin",0)
	Function GetRs(Byval Sql,Byval K)
		Dim TpRStr,TpRK
		On Error Resume Next
		TpRK = CLng(K)
		AB.Use("db")
		TpRStr = AB.db.GRS(sql)(TpRK)
		GetRs = TpRStr
		On Error Goto 0
	End Function

	'转化字符串为首字母大写,剩下的全小写
	Function MCase(Byval Key)
		On Error Resume Next
		IF AB.C.IsNul(Key) Or Len(Key)<=0 Then:MCase="":Exit Function:End IF
		'Key=LCase(Key)
		Dim KeyA,KeyB
		IF Len(Key)>0 Then
			KeyA=Left(Key,1)
			KeyA=UCase(KeyA)
		End IF
		IF Len(Key)>1 Then
			KeyB=Right(Key,Len(Key)-1)
			KeyB=LCase(KeyB)
		End IF
		MCase = KeyA & KeyB
		On Error Goto 0
	End Function

	'字符串转化为变量值
	Function GetVar(Byval var)
		On Error Resume Next
		Execute("Function ljx_get_value(): ljx_get_value=" & var  & ": end Function")
		GetVar = ljx_get_value()
		'GetVar = Eval(var)
		On Error Goto 0
	End function

	' === e.g. ===
	' @ 轻松实现变量名的转化
	' dim username,password
	' username = "admin"
	' password = "123456"
	' ab.c.print ab.d.Var2Value("sql: update users set pwd='{password}' where name='{username}'")
	' 返回结果: "sql: update users set pwd='123456' where name='admin'"
	Function Var2Value(Byval var)
		Var2Value = Txt2Value(var, 0)
	End Function

	Function Txt2Value(Byval str, Byval p)
		dim RegEx, Matches, Result
		Set RegEx = new RegExp
		Select Case p
			Case 0 RegEx.Pattern = "\{(\w+)\}"              '变量名有效
			Case 1 RegEx.Pattern = "\{([\w+\-\*/\\<>=]+)\}" '变量名及运算符有效
			'Case 2 RegEx.Pattern = "\{([\w\s]+)\}"   		'除换行符外的所有字符有效
			Case Else Exit Function
		End Select
		RegEx.IgnoreCase = True
		RegEx.Global = True
		Set Matches = RegEx.Execute(str)
		Result = str
		For Each Match In Matches
			Result = Replace(Result, Match.Value, GetVar(Match.SubMatches(0)))
		Next
		set Matches = nothing
		set RegEx = nothing
		Txt2Value = Result
	End Function

	Function QueryParam(oStr) '将指定的URL参数存储为数组，每个用(,)隔开 如 AB.D.QueryParam("id,sort,c_id")
		On Error Resume Next
		Dim A(),I,K:K=0
		IF IsNull(oStr) Or Trim(oStr)="" Then:Exit Function:End IF
		oStr = AB.C.RP(oStr, "|", ",")
		For I=0 To Ubound(Split(oStr,","))
			Redim Preserve A(K)
			IF Trim(Split(oStr,",")(I))<>"" Then
				A(K) = Request.QueryString(Cstr(Trim(Split(oStr,",")(I))))
				K=K+1
			End IF
		Next
		QueryParam = A
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.RQ(q, url)
	'@ 返  回:  String
	'@ 作  用:  获取url参数(可指定特定的URL)，若指定的URL为空则默认为当然访问页面URL
	'==DESC=====================================================================================
	'@ 参数 q 	: 地址参数
	'@ 参数 url : 指定的URL地址，为空则默认为当然访问页面URL
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.D.RQ("id", "http://localhost/test.html?act=dl&id=5") '返回：5
	'@ 假设当前页面地址：http://127.0.0.1/test.asp?cid=3 ，那么 AB.D.RQ("cid", "") 值返回：3
	'@ *****************************************************************************************

	Function RQ(Byval q, Byval url)
		RQ = ""
		If Trim(q)="" Then Exit Function
		If IsNull(url) Or Trim(url)="" Then url = "?" & Request.QueryString
		If Instr(url,"?")>0 then
			Dim temp:temp = split(url,"?")(1)
			Dim i,a : a = split(temp,"&")
			For i=0 To UBound(a)
				If LCase(Split(a(i),"=")(0)) = LCase(q) Then
					If Instr(a(i),"=")>0 then
						RQ = Split(a(i),"=")(1)
						Exit For
					End If
				End If
			Next
		End If
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.D.MultiExec(sfun, arg, n) 多次执行函数简单化
	'@ 返  回:  [Any]
	'@ 作  用:  把多次执行函数简单化一次性写成
	'@ 			缺点：只能执行只有一个参数的函数,且此函数不能有语法错误
	'@ 			      且对于类里面执行函数则必须完整写出外部定义的类名前缀如(mycls.myfun)
	'==DESC=====================================================================================
	'@ 参数 sfun : 执行的函数名
	'@ 参数 arg  : 执行的函数参数
	'@ 参数 n 	 : 执行次数
	'==DEMO=====================================================================================
	'@ e.g.
	'@ 如对于函数: Function myfun(s) : myfun = s & "+" : End Function
	'@ Dim t : t = "b" : t = myfun(t) : t = myfun(t) : t = myfun(t) : t = myfun(t)
	'@ 上面可以简写成: Dim t : t = AB.D.MultiExec("myfun","b", 4)
	'@ *****************************************************************************************

	Function MultiExec(Byval sfun, Byval arg, Byval n)
		On Error Resume Next
		Dim I,sArg,TpRs
		If AB.C.IsInt(arg) Then sArg = arg Else sArg = """" & arg & """"
		If CLng(n)>0 Then
			For I=1 To CLng(n)
				If I=1 Then
					Execute("TpRs = " & sfun & "(" & sArg &")")
					'TpRs = Eval("" & sfun & "(" & sArg &")")
				Else
					If AB.C.IsInt(TpRs) Then TpRs = TpRs Else TpRs = """" & TpRs & """"
					Execute("TpRs = " & sfun & "(" & TpRs &")")
					'TpRs = Eval("" & sfun & "(" & TpRs &")")
				End If
			Next
		ElseIf CLng(n)=0 Then
			TpRs = arg
		End If
		MultiExec = TpRs
		On Error Goto 0
	End Function

End Class
%>