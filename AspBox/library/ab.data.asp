<%
'######################################################################
'## ab.data.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Dictionary Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2013/03/11 11:58
'## Description :   AspBox字典库操作类
'######################################################################

Class Cls_AB_Data

	Private o_dict
	Public Items, Data

	Private Sub Class_Initialize()
		On Error Resume Next
		Set o_dict 		= Server.CreateObject(AB.dictName)
		Set Items 		= o_dict
		Set Data 		= o_dict
		On Error GoTo 0
	End Sub

	Private Sub Class_Terminate()
		Free()
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Data.New 方法
	'# @syntax: Set d = AB.Data.New
	'# @return: Object (ASP对象)
	'# @dowhat: 建立字典操作对象, 创建一个 AspBox_Data 对象
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# none
	'------------------------------------------------------------------------------------------

	Public Function [New]()
		Set [New] = New Cls_AB_Data
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Data.Count 属性
	'# @syntax: n = AB.Data.Count
	'# @return: Integer (整型)
	'# @dowhat: 获取存储的key子项个数
	'--DESC------------------------------------------------------------------------------------
	'# @param n : Variant(已定义的变量)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Data.Clear '清空数据
	'# AB.Data("a") = "test123"
	'# AB.Data("b") = AB.Dict
	'# AB.C.PrintCn AB.Data.Count '计算个数: 2
	'------------------------------------------------------------------------------------------

	Public Property Get Count()
		Count = o_dict.Count
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Data.Item 方法
	'# @syntax: AB.Data.Item(str)[ = value]
	'# @alias:  AB.Data(str)[ = value]
	'# @return: Integer (整型)
	'# @dowhat: 设置或获取字典项值
	'--DESC------------------------------------------------------------------------------------
	'# @param str :  String (字符串) 存储的键名
	'# @param value(可选) : Any (任意值) 存储值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Data("a") = "test123" '等效于：AB.Data("a").Set "test123"
	'# AB.C.PrintCn AB.Data("a") '字符串数据可以直接获取
	'# AB.Data("b") = AB.Dict '存储对象
	'# AB.Trace AB.Data("b") '读取对象值
	'------------------------------------------------------------------------------------------

	Public Property Let Item(ByVal k, ByVal v)
		'o_dict(k) = v
		[Set] k, v
	End Property
	Public Default Property Get Item(ByVal k)
		If IsObject(o_dict(k)) Then Set Item = o_dict(k) Else Item = o_dict(k)
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Data.Keys 属性
	'# @syntax: n = AB.Data.Keys
	'# @return: Array (数组)
	'# @dowhat: 返回一个包含所有项键名的数组
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Data.Add "a","abc123"
	'# AB.Data("b") = Array()
	'# AB.Data("c") = AB.Dict
	'# AB.C.PrintCn AB.Data.Count '计算项个数
	'# Dim a : a = AB.Data.Keys() '返回一个包含所有项键名的数组
	'# AB.C.PrintCn "所有子项键名："
	'# Dim i
	'# For Each i In a
	'# 	AB.C.PrintCn i
	'# Next
	'# Dim k : k = "a"
	'# AB.C.PrintCn "子项键名为 "& k &" 的值："
	'# AB.Trace AB.Data(k)
	'------------------------------------------------------------------------------------------

	Public Property Get Keys()
		Dim i, n, a : a = Array()
		n = 0
		For Each i In o_dict
			Redim Preserve a(n)
			a(n) = i
			n = n+1
		Next
		Keys = a
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Data.Add 方法
	'# @syntax: AB.Data.Add str, value
	'# @alias:  AB.Data.Set str, value
	'# @return: Void
	'# @dowhat: 存储键名为str的值到字典里
	'--DESC------------------------------------------------------------------------------------
	'# @param str : String (字符串) 存储的键名
	'# @param value(可选) : Any (任意值) 存储值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Data.Add "a", "test123" '等同于：AB.Data.Set "a", "test123"
	'# AB.C.PrintCn AB.Data("a")
	'# AB.Trace AB.Data("a")
	'------------------------------------------------------------------------------------------

	Public Sub Add(ByVal k, ByVal v)
		If Not IsObject(o_dict) Then Set o_dict = Server.CreateObject(AB.dictName)
		If Not o_dict.Exists(k) Then
			o_dict.add k, v
		Else
			If IsObject(v) Then
				Set o_dict(k) = v
			Else
				If IsObject(o_dict(k)) Then o_dict(k) = Empty
				o_dict(k) = v
			End If
		End IF
	End Sub
	Public Sub [Set](ByVal k, ByVal v) : Add k, v : End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Data.Remove 方法
	'# @syntax: AB.Data.Remove str
	'# @alias:  AB.Data.Del str
	'# @return: Void
	'# @dowhat: 从字典里删除键名为str项
	'--DESC------------------------------------------------------------------------------------
	'# @param str : String (字符串) 键名
	'--DEMO------------------------------------------------------------------------------------
	'# Dim d : Set d = AB.Data.New
	'# d.add "a","abc123"
	'# d("b") = Array()
	'# d("c") = AB.Dict
	'# AB.Trace d("b")
	'# AB.Trace d("c")
	'# d.Del "c"
	'# If Not d.Items.Exists("c") Then AB.C.PrintCn "项 c 已被删除"
	'# AB.C.PrintCn "共" & d.Count & "项"
	'# Dim i
	'# For Each i In d.Keys
	'# 	AB.C.PrintCn "子项键名为 "& i &" 的值："
	'# 	AB.Trace d(i)
	'# Next
	'------------------------------------------------------------------------------------------

	Public Sub Remove(ByVal k)
		If o_dict.Exists(k) Then
			o_dict.Remove(k)
		End IF
	End Sub
	Public Sub Del(ByVal k) : Remove k : End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Data.Clear 方法
	'# @syntax: AB.Data.Clear
	'# @alias:  AB.Data.RemoveAll
	'# @return: Void
	'# @dowhat: 清空所有字典项
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# Dim d : Set d = AB.Data.New
	'# d.add "a","abc123"
	'# d("b") = Array()
	'# d("c") = AB.Dict
	'# d.Clear
	'# AB.Trace d.Count '计算项个数
	'------------------------------------------------------------------------------------------

	Public Sub Clear()
		Dim i
		For Each i In o_dict
			If IsObject(o_dict(i)) Then Set o_dict(i) = Nothing Else o_dict(i) = Empty
		Next
		o_dict.RemoveAll
		Items.RemoveAll
		Data.RemoveAll
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Data.Free 方法
	'# @syntax: AB.Data.Free()
	'# @return: Void
	'# @dowhat: 清空所有字典项,释放资源(一般是结束时才释放内存)
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Data.Free()
	'------------------------------------------------------------------------------------------

	Public Sub Free()
		Clear()
		Set o_dict = Nothing
		Set Items = Nothing
		Set Data = Nothing
	End Sub

End Class
%>