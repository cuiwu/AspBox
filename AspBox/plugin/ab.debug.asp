<%
'#################################################################################
'## ab.debug.asp
'## ------------------------------------------------------------------------------
'## Feature     : AspBox Debug Plugin
'## Version     : v1.0.1
'## Author      : Lajox(lajox@19www.com)
'## Update Date : 2012-12-21 2:40
'## Description : 
' 此插件用于输出调试ASP某些元素，如：
'   AB.Ext("Debug").ShowCookies => 输出全部Cookie
'   AB.Ext("Debug").ShowCaches => 输出全部缓存
'   AB.Ext("Debug").ShowForms => 输出全部表单元素
'   AB.Ext("Debug").ShowAll => 输出全部信息
'#################################################################################

Class Cls_AB_Debug

	Private tpl, o_conn
	Private oForm, oFile, oStorage
    Private dteRequestTime, dteFinishTime
	Private appTime
    Private b_enabled
    Private s_tplTagMask, s_tplLoadStyle, s_tplLoadStr, s_tplLoadStr2

	Private Sub Class_Initialize()
		On Error Resume Next
		appTime = 1048
		b_enabled = False
		dteRequestTime = Now()
		Set oForm = Server.CreateObject(AB.DictName)
		oForm.CompareMode = 1
		Set oFile = Server.CreateObject(AB.DictName)
		oFile.CompareMode = 1
        Set oStorage = Server.CreateObject(AB.DictName)
		oFile.CompareMode = 1
		s_tplTagMask = "{{*}}"
		s_tplLoadStyle = "<style>.ab-trace{width:90%;font-size:12px;font-family:Consolas;margin:10px auto;padding:0;background-color:#FFF;}.ab-trace h3,.ab-trace h4{font-size:12px;margin:0;line-height:24px;text-align:center;background-color:#999;border:1px solid #555;color:#FFF;border-bottom:none;}.ab-trace h4{padding:5px;line-height:1.5em;text-align:left;background-color:#EBF0F5;color:#000; font-weight:normal;}.ab-trace h4 strong{color:red;}.ab-trace table{width:100%;margin:0;padding:0;border-collapse:collapse;border:1px solid #555;border-bottom:none;}.ab-trace th{background-color:#EEE;white-space:nowrap;}.ab-trace thead th{background-color:#CCC;}.ab-trace th,.ab-trace td{font-size:12px;border:1px solid #999;padding:4px;word-break:break-all;}.ab-trace span.info{color:#F30;}.ab-trace i{color:#F15C3A;font-style:normal;}.ab-trace s{color:#EDA731;text-decoration:none;}.ab-trace em{color:#407AB3;font-style:normal;}</style>"
		s_tplLoadStr = s_tplLoadStyle & "<div class=""ab-trace""><h4><strong>{{type}}</strong> ，{{#if @count!=''}}共有 <strong>{{count}}</strong> 条数据，{{/#if}}以下是其中的{{#if @top>0}}前 <strong>{{top}}</strong> 条{{#else}}数据{{/#if}}：</h4>{{table}}</div>"
		s_tplLoadStr2 = s_tplLoadStyle & "<div class=""ab-trace"">{{table}}</div>"
	End Sub

	Private Sub Class_Terminate()
		Set oForm = Nothing
		Set oFile = Nothing
		Set oStorage = Nothing
		If isObject(tpl) Then Set tpl = Nothing
		On Error Goto 0
	End Sub

	Private Function GetTable(ByVal n)
		Select Case n
			Case 0 : GetTable = "<table><thead><tr><th width=""20%"">{{cname}}</th><th width=""80%"">{{cvalue}}</th></tr></thead>{{#:loop}}<tr><th>{{name}}</th><td>{{value}}</td></tr>{{/#:loop}}</table>"
			Case 1 : GetTable = "<table><thead><tr><th width=""5%"">{{cno}}</th><th width=""15%"">{{cname}}</th><th width=""80%"">{{cvalue}}</th></tr></thead>{{#:loop}}<tr><th>{{no}}</th><th>{{name}}</th><td>{{value}}</td></tr>{{/#:loop}}</table>"
			Case 2 : GetTable = "<table><thead><tr><th width=""3%"">{{cno}}</th>{{#:col}}<th>{{field}}</th>{{/#:col}}</tr></thead>{{#:rs}}<tr><th>{{i}}</th>{{#:fields}}<td>{{value}}</td>{{/#:fields}}</tr>{{/#:rs}}</table>"
			Case 3 : GetTable = "<table><thead><tr><th width=""20%"">{{cname}}</th><th width=""80%"">{{cvalue}}</th></tr></thead></table>{{#:rs}}<h4>第<strong> {{i}} </strong>条数据：</h4><table>{{#:loop}}<tr><th width=""20%"">{{name}}</th><td width=""80%"">{{value}}</td></tr>{{/#:loop}}</table>{{/#:rs}}"
			Case 4 : GetTable = "<table><tr><td>{{value}}</td></tr></table>"
			Case 5 : GetTable = "<h4>{{info}}</h4>"
			Case 6 : GetTable = "{{#:table}}<h4><strong>{{tableorview}}：{{name}}</strong></h4><table><thead><tr><th width=""20%"">字段名</th><th width=""20%"">字段类型/大小</th><th width=""10%"">允许空</th><th width=""10%"">默认值</th><th width=""40%"">说明</th></tr></thead>{{#:loop}}<tr><th>{{field}}</th><td style=""text-align:center;"">{{datatype}}</td><td style=""text-align:center;"">{{nullable}}</td><td style=""text-align:center;"">{{default}}</td><td>{{desc}}</td></tr>{{/#:loop}}</table>{{/#:table}}"
		End Select
	End Function

	'监听变量等控制开关
	Public Property Let IfEnabled(ByVal b)
		b_enabled = b
	End Property

	Public Property Get IfEnabled
		IfEnabled = b_enabled
	End Property

	'添加监听变量
    Public Sub list(Byval o)
		On Error Resume Next
		Dim i, e, t, arr : arr = Array()
        If IfEnabled Then
			arr = getArr_(o)
			For Each e In arr
				If oStorage.Exists(Lcase(e)) Then
					oStorage(Lcase(e)) = eval(""&e)
				Else
					oStorage.add Lcase(e), eval(""&e)
				End IF
			Next
        End If
		On Error Goto 0
    End Sub
	Public Sub add(Byval o)
		list(o)
    End Sub

	'移除监听变量
    Public Sub del(Byval o)
		On Error Resume Next
		Dim i, e, t, arr : arr = Array()
        If IfEnabled Then
			arr = getArr_(o)
			For Each e In arr
				If oStorage.Exists(Lcase(e)) Then
					oStorage.Remove(Lcase(e))
				End IF
			Next
        End If
		On Error Goto 0
    End Sub

	'移除全部监听变量
    Public Sub DelAll()
		On Error Resume Next
        If IfEnabled Then
			oStorage.RemoveAll
        End If
		On Error Goto 0
    End Sub

	'获取监听的变量字典库
	Public Property Get VarDict
		On Error Resume Next
		Set VarDict = oStorage
	End Property

	'清空全部Cookie
	Public Sub RemoveCookies()
		AB.Use "Cookie"
		AB.Cookie.RemoveAll()
	End Sub

	Private Sub initTpl()
		AB.Use "Tpl"
		Set tpl = AB.Tpl.New
		tpl.TagMask = s_tplTagMask
		tpl.LoadStr s_tplLoadStr
		tpl "top", 0
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowCookies()
	'# @return: void
	'# @dowhat: 输出全部Cookie
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowCookies()
	'------------------------------------------------------------------------------------------

	Public Sub ShowCookies()
		ShowCollection "Response.Cookies变量", 4
	End Sub

	'===缓存操作部分===

	'设置缓存时间
	Public Sub SetCacheTime(ByVal T)
		appTime = T
		IF IsNull(appTime) Then appTime = 1048 '(分钟)
	End Sub

	'设置缓存(值)
	Public Sub SetCache(ByVal AppName,ByRef AppData)
		AB.C.SetApp AppName,AppData
	End Sub

	'获取缓存(值)
	Public Function GetCache(AppName)
		GetCache = AB.C.GetApp(AppName)
	End Function

	'删除缓存
	Public Sub DelCache(ByVal AppName)
		AB.C.RemoveApp AppName
	End Sub

	'清空全部缓存
	Public Sub DelAllCache()
		Application.Lock()
		Application.Contents.ReMoveAll()
		Application.UnLock()
	End Sub
	
	'检查缓存是否可用(过期)
	Public Function ValidCache(MyCacheName) 
		ValidCache = False
		CacheValue = Application(MyCacheName)
		IF Not IsArray(CacheValue) Then Exit Function
		IF Not IsDate(CacheValue(1)) Then Exit Function
		IF DateDiff("s", CDate(CacheValue(1)), Now()) < 60 * appTime  Then
			ValidCache = True
		End IF
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowCaches()
	'# @return: void
	'# @dowhat: 输出全部缓存
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowCaches()
	'------------------------------------------------------------------------------------------

	Public Sub ShowCaches()
		ShowCollection "Application.Contents变量", 8
	End Sub

	'===表单操作部分===

	'获取表单元素值
	Public Function Form(s)
		AB.Use "Form"
		AB.Form.Init()
		Form = AB.Form.FormVar(s)
	End Function

	'获取表单EncType的值
	Public Function FormEncType()
		AB.Use "Form"
		AB.Form.Init()
		FormEncType = AB.Form.FormEncType
	End Function

	'获取表单类型,返回值为file或text
	Public Function FormType()
		AB.Use "Form"
		AB.Form.Init()
		FormType = AB.Form.FormType
	End Function

	'输出全部表单元素(排除文件元素)
	Public Sub ShowForms()
		AB.Use "Form"
		AB.Form.Init()
		AB.Form.ShowForms()
	End Sub

	'输出全部表单的文件元素
	Public Sub ShowFiles()
		AB.Use "Form"
		AB.Form.Init()
		AB.Form.ShowFiles()
	End Sub

	'===调试输出===

	'------------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowAll()
	'# @return: void
	'# @dowhat: 调试输出全部信息
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowAll()
	'------------------------------------------------------------------------------------------

    Public Sub ShowAll()
        If IfEnabled Then
            ShowCollection "SUMMARY INFO", 0
        End If
		ShowCollection "VARIABLE STORAGE", 1
		ShowCollection "QUERYSTRING COLLECTION", 2
		ShowCollection "FORM COLLECTION", 3
		ShowCollection "COOKIES COLLECTION", 4
		ShowCollection "SERVER VARIABLES COLLECTION", 5
		ShowCollection "SESSION CONTENTS COLLECTION", 6
		ShowCollection "SESSION STATICOBJECTS COLLECTION", 7
		ShowCollection "APPLICATION CONTENTS COLLECTION", 8
		ShowCollection "APPLICATION STATICOBJECTS COLLECTION", 9
    End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowVars()
	'# @return: void
	'# @dowhat: 调试输出监听变量
	'--DESC------------------------------------------------------------------------------------
	'# @param : none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowVars()
	'------------------------------------------------------------------------------------------

    Public Sub ShowVars()
        ShowCollection "VARIABLE STORAGE", 1
    End Sub

	Public Sub ShowCollection(Byval Name, Byval Style)
		On Error Resume Next
        dteFinishTime = Now()
		Dim i, x, y
		initTpl()
		Select Case Style
			Case 0:
				tpl "type", Name
				tpl.TagStr "table", GetTable(0)
				tpl "cname", "名称"
				tpl "cvalue", "值"
				tpl "name", "<s>Time of Request</i>"
				tpl "value", "<i>"& dteRequestTime &"</i>"
				tpl.Update "loop"
				tpl "name", "<s>Time Finished</s>"
				tpl "value", "<i>"& dteFinishTime &"</i>"
				tpl.Update "loop"
				tpl "name", "<s>Elapsed Time</s>"
				tpl "value", "<i>"& DateDiff("s", dteRequestTime, dteFinishTime) &"</i>"
				tpl.Update "loop"
				tpl "name", "<s>Request Type</s>"
				tpl "value", "<i>"& Request.ServerVariables("REQUEST_METHOD") &"</i>"
				tpl.Update "loop"
				tpl "name", "<s>Status Code</s>"
				tpl "value", "<i>"& Response.Status &"</i>"
				tpl.Update "loop"
			Case 1: '输出 【Me.VarDict】
				tpl "type", Name
				'tpl "count", Me.VarDict.Count
				If Me.VarDict.Count = 0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "暂无任何监听变量"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "名称"
					tpl "cvalue", "值"
					For Each i In Me.VarDict
						tpl "name", i
						ShowValueDeep Me.VarDict.Item(i)
						tpl.Update "loop"
					Next
				End If
			Case 2: '输出 【Request.QueryString】
				tpl "type", Name
				'tpl "count", Request.QueryString.Count
				If Request.QueryString.Count = 0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "没有任何QueryString参数被传递"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "名称"
					tpl "cvalue", "值"
					For Each i In Request.QueryString
						tpl "name", i
						tpl "value", AB.C.HtmlEncode(Request.QueryString(i))
						tpl.Update "loop"
					Next
				End If
			Case 3: '输出 【Request.Form】
				tpl "type", Name
				'tpl "count", Request.Form.Count
				If Request.Form.Count = 0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "没有任何表单数据被提交"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "名称"
					tpl "cvalue", "值"
					For Each i In Request.Form
						tpl "name", i
						tpl "value", AB.C.HtmlEncode(Request.Form(i))
						tpl.Update "loop"
					Next
				End If
			Case 4: '输出 【Request.Cookies】
				tpl "type", Name
				'tpl "count", Request.Cookies.Count
				If Request.Cookies.Count = 0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "您的电脑上没有任何本站的Cookies数据"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "Cookie名称"
					tpl "cvalue", "Cookie值"
					For Each x In Request.Cookies
						If Request.Cookies(x).HasKeys Then '判断是否带有键(Keys)
							tpl "name", "<i>"&x&"</i>"
							tpl "value", AB.C.HtmlEncode(Request.Cookies(x))
							tpl.Update "loop"
							For Each Y In Request.Cookies(x)
								tpl "name", " : <s>"&y&"</s>"
								tpl "value", AB.C.HtmlEncode(Request.Cookies(x)(y))
								tpl.Update "loop"
							Next
						Else
							tpl "name", "<i>"&x&"</i>"
							tpl "value", AB.C.HtmlEncode(Request.Cookies(x))
							tpl.Update "loop"
						End If
					Next
				End If
			Case 5: '输出 【Request.ServerVariables】
				tpl "type", Name
				'tpl "count", Request.ServerVariables.Count
				tpl.TagStr "table", GetTable(0)
				tpl "cname", "名称"
				tpl "cvalue", "值"
				For Each i In Request.ServerVariables
					tpl "name", i
					tpl "value", AB.C.HtmlEncode(Request.ServerVariables(i))
					tpl.Update "loop"
				Next
			Case 6: '输出 【Session.Contents】
				tpl "type", Name
				'tpl "count", Session.Contents.Count
				tpl.TagStr "table", GetTable(0)
				tpl "cname", "Session名称"
				tpl "cvalue", "Session值"
				UpdateLoop "Session.CodePage"
				UpdateLoop "Session.LCID"
				UpdateLoop "Session.SessionID"
				UpdateLoop "Session.Timeout"
				For Each i In Session.Contents
					tpl "name", "Session("""&i&""")"
					ShowValue Session(i)
					tpl.Update "loop"
				Next
			Case 7: '输出 【Session.StaticObjects】
				tpl "type", Name
				'tpl "count", Session.StaticObjects.Count
				If Session.StaticObjects.Count=0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "目前没有任何数据"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "名称"
					tpl "cvalue", "值"
					For Each i In Session.StaticObjects
						tpl "name", i
						ShowValue Session.StaticObjects(i)
						tpl.Update "loop"
					Next
				End If
			Case 8: '输出 【Application.Contents】
				tpl "type", Name
				'tpl "count", Application.Contents.Count
				If Application.Contents.Count=0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "目前没有任何缓存"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "缓存名称"
					tpl "cvalue", "缓存值"
					For Each i In Application.Contents
						tpl "name", i
						ShowValue Application(i)
						tpl.Update "loop"
					Next
				End If
			Case 9: '输出 【Application.StaticObjects】
				tpl "type", Name
				'tpl "count", Application.StaticObjects.Count
				If Application.StaticObjects.Count=0 Then
					tpl.TagStr "table", GetTable(4)
					tpl "value", "目前没有任何数据"
				Else
					tpl.TagStr "table", GetTable(0)
					tpl "cname", "名称"
					tpl "cvalue", "值"
					For Each i In Application.StaticObjects
						tpl "name", i
						ShowValue Application.StaticObjects(i)
						tpl.Update "loop"
					Next
				End If
			Default:
				'none
		End Select
		AB.C.Print tpl.GetHtml()
		On Error Goto 0
	End Sub

	'===数组操作部分===

	'------------------------------------------------------------------------------------------
	'# AB.Ext("Debug").ShowArray(arr)
	'# @return: void
	'# @dowhat: 按形式输出(一维)数组元素值
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组)
	'--DEMO------------------------------------------------------------------------------------
	'# dim c(2,2)
	'# c(0,0) = "0-0" : c(0,1) = "0-1" : c(0,2) = "0-2"
	'# c(1,0) = "1-0" : c(1,1) = "1-1" : c(1,2) = "1-2"
	'# c(2,0) = "2-0" : c(2,1) = "2-1" : c(2,2) = "2-2"
	'# dim d(2) : d(0) = "0" : d(1) = "1" : d(2) = "2"
	'# dim e(4,5,8)
	'# 'AB.Trace c
	'# 'AB.Ext("Debug").ShowArray(c)
	'# Dim debug : Set debug = AB.Ext("Debug")
	'# debug.ShowArray(c)
	'------------------------------------------------------------------------------------------

	Public Sub ShowArray(Byval arr)
		Dim aStr
		aStr = getArrayStr(arr)
		AB.C.Print aStr
	End Sub

	'===以下辅助函数===

	Private Function getArr_(Byval o)
		On Error Resume Next
		Dim i, e, t, arr : arr = Array()
		If IsArray(o) Then
			arr = o
		Else
			If Instr(o, ",")<0 Then
				ReDim Preserve arr(0)
				arr(0) = Trim(o)
			Else
				t = Split(o, ",")
				For i=0 To Ubound(t)
					If Trim(t(i))<>"" Then
						ReDim Preserve arr(i)
						arr(i) = Trim(t(i))
					End If
				Next
			End If
		End If
		getArr_ = arr
		On Error Goto 0
	End Function

	Private Function ShowValue(ByVal o)
		If IsObject(o) Then
			tpl.Tag("value") = "<span class=""info"">[ "&TypeName(o)&" Object ]</span>"
		ElseIf IsArray(o) Then
			tpl.Tag("value") = "<span class=""info"">[ Array ]</span>"
		Else
			tpl "value", AB.C.HtmlEncode(o)
		End If
	End Function

	Private Function ShowValueDeep(ByVal o)
		Dim e, temp, estr, k : k = 0
		If IsObject(o) Then
			tpl.Tag("value") = "<span class=""info"">[ "&TypeName(o)&" Object ]</span>"
		ElseIf IsArray(o) Then
			tpl.Tag("value") = getArrayStr(o)
		Else
			tpl "value", AB.C.HtmlEncode(o)
		End If
	End Function

	Private Function getArrayStr(ByVal o)
		On Error Resume Next
		Dim e, temp, estr, k : k = 0
		Dim i, j, s, t : i = 0 : j = 0
		If IsArray(o) Then
			AB.Use "A"
			If AB.A.Size(o) = 0 Then '一维数组
				estr = "<span class=""info"">[ Array ] : </span><em>Array()</em>"
			ElseIf AB.A.Size(o) = 1 Then '一维数组
				For Each e In o
					temp = ""
					If IsObject(e) Then
						temp = "[ "&TypeName(e)&" Object ]"
					ElseIf IsArray(e) Then
						s = "" : j = 0
						For Each i In e
							If IsObject(i) Then
								t = "[ "&TypeName(i)&" Object ]"
							ElseIf IsArray(i) Then
								t = "[ Array ]"
							ElseIf AB.C.isInt(i) Then
								t = i
							Else
								t = """" & i & """"
							End If
							If j = 0 Then s = s & "" & t Else s = s & ", " & t
							j = j + 1
						Next
						temp = "Array(" & s & ")"
					Else
						If AB.C.isInt(e) Then
							temp = e
						Else
							temp = """" & e & """"
						End If
					End If
					If k = 0 Then estr = estr & "" & temp Else estr = estr & ", " & temp
					k = k + 1
				Next
				estr = "Array( " & estr & " )"
				estr = "<span class=""info"">[ Array ] : </span><em>" & estr & "</em>"
			ElseIf AB.A.Size(o) = 2 Then '二维数组
				Dim size1, size2
				size1 = Ubound(o)
				size2 = Ubound(o,2)
				AB.Use "Tpl"
				Dim tpl2 : Set tpl2 = AB.Tpl.New
				tpl2.TagMask = s_tplTagMask
				tpl2.LoadStr s_tplLoadStr2
				tpl2.TagStr "table", GetTable(3)
				tpl2 "cname", "下标"
				tpl2 "cvalue", "元素值"
				For i = 0 To size2
					tpl2 "i", i+1
					For j = 0 To size1
						tpl2 "name", "("&j&", "&i&")"
						If IsObject(o(j,i)) Then
							tpl2.Tag("value") = "<span class=""info"">[ "&TypeName(o(j,i))&" Object ]</span>"
						ElseIf IsArray(o(j,i)) Then
							tpl2.Tag("value") = "<span class=""info"">[ Array ]</span>"
						Else
							tpl2 "value", AB.C.HtmlEncode(o(j,i))
						End If
						tpl2.Update "loop"
					Next
					tpl2.Update "rs"
				Next
				estr = tpl2.GetHtml
				Set tpl2 = Nothing
				estr = "<span class=""info"">[ Array ] : </span><br>" & VBCrlf & estr & ""
			ElseIf AB.A.Size(o) > 2 Then '二维数组以上
				estr = "<span class=""info"">[ Array ] <em><font color=red>" & AB.A.Size(o) & "</font>维数组</em></span>"
			Else
				estr = "<span class=""info"">[ Array ]</span>"
			End If
			GetArrayStr = estr
		Else
			GetArrayStr = ""
		End If
		On Error Goto 0
	End Function

	Private Sub UpdateLoop(ByVal s)
		tpl "name", "<span class=""info"">"&s&"</span>"
		tpl "value", "<span class=""info"">"&Eval(s)&"</span>"
		tpl.Update "loop"
	End Sub

End Class
%>