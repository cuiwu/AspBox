<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
'vb变量:  vb数组, vb对象(字典), vb日期或时间, vb的Null值
Dim vbArr, vbObj, vbDate, vbNulls
vbArr = Array(1,"x",Array(7,4)) 'vb数组
Set vbObj = Server.CreateObject("Scripting.Dictionary") 'vb字典对象
vbObj("x") = 5 : vbObj("y") = "test"
vbDate = #1/1/13# 'vbDate = Date(), vbDate = Now(), vbDate = CDate("2013-1-1") 'vb日期或时间
vbNulls = Null 'vb的Null值
AB.C.PrintCn vbArr(1) '输出：x
AB.C.PrintCn vbArr(2)(0) '输出：7
AB.C.PrintCn vbObj("x") '输出：5
AB.C.PrintCn vbObj("y") '输出：test
AB.C.PrintCn vbDate '输出：2012-9-8 下午 07:18:25
AB.C.PrintCn vbNulls '输出内容为空

AB.C.PrintCn "===================="
'js变量： js数组, js对象, js日期时间, js的Null值
Dim jsArr, jsObj, jsDate, jsNulls
ab.use "sc" : dim sc : Set sc = ab.sc.new
sc.Lang = "js"
sc.Add "function getJsArr(){ return [1,'x',[7,4]]; }"
sc.Add "function getJsObj(){ return {'x':5, 'y':'test'}; }"
sc.Add "function getJsDate(){ return (new Date()); }"
sc.Add "function getJsNull(){ return null; }"
Set jsArr = sc.eval("getJsArr()") 'js数组
Set jsObj = sc.eval("getJsObj()") 'js对象
jsDate = sc.eval("getJsDate()") 'js日期时间
jsNulls = sc.eval("getJsNull()") 'js日期时间
AB.C.PrintCn jsArr.[1] '输出：x
AB.C.PrintCn jsArr.[2].[0] '输出：7
AB.C.PrintCn jsObj.x '输出：5
AB.C.PrintCn jsObj.y '输出：test
AB.C.PrintCn jsDate '输出：Sat Sep 8 19:18:25 UTC+0800 2012
AB.C.PrintCn jsNulls '输出内容为空

AB.C.PrintCn "===================="

sc.Add "function foo(){ var person = {name: {a:""zhangsan""}, pass: ""123"", fn: function(){alert(this.pass);} }; return person; }"
Dim myData : Set myData = sc.eval("foo()")
AB.Trace myData

ab.use "json"
ab.trace AB.Json.toJson(myData)
ab.trace AB.Json.jsEval("[1,'x',[7,4]]")
ab.trace AB.Json.jsEval("{'x':5, 'y':'test'}")
ab.trace AB.Json.jsEval("(new Date()).toString()")
ab.trace AB.Json.jsEval("null")
%>