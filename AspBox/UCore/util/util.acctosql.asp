<%
'######################################################################
'## util.acctosql.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc AccToSQL-Util Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/06/10 23:21
'## Description :   AspBox Mvc AccToSQL-Util Block(access数据库升迁(Mssql)工具拓展模块)
'######################################################################

Class Cls_Util_AccToSQL

	Private cfg_dbtype, cfg_accdb, cfg_sqldb, cfg_sqluid, cfg_sqlpwd
	Private temp_accdb, temp_sqldb, temp_sqluid, temp_sqlpwd, temp_sapass
	Private tpl_header, tpl_main, tpl_footer
	Private Conn,ConnStr
	Public enMode,uiMode

	Private Sub Class_Initialize()
		cfg_dbtype = 1 '数据库类型：1:sql, 0:access
		cfg_accdb = "/data/mydb.mdb"
		cfg_sqldb = "myData"
		cfg_sqluid = "sa"
		cfg_sqlpwd = "123456"
		temp_accdb = cfg_accdb
		temp_sqldb = cfg_sqldb
		temp_sqluid = cfg_sqluid
		temp_sqlpwd = cfg_sqlpwd
		temp_sapass = cfg_sqlpwd
		enMode = 0
		InitTpl()
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'@ *****************************************************************************
	'@ 过程名:  Util.AccToSQL.ShowPage()
	'@ 返  回:  无返回值
	'@ 作  用:  “数据库生迁脚本编写器”主页面显示
	'==Param========================================================================
	'@ 参数 : 无
	'==DEMO=========================================================================
	'@ ab.use "mvc" : AB.C.Clear : Util.Lib("AccToSQL").ShowPage()
	'@ *****************************************************************************

	Sub ShowPage()
		If Request.Form<>"" and Request.QueryString("action")<>"" Then
			dim form_dbname,form_enMode,form_uiMode,form_sqluid,form_sqldb,form_sqlpwd,form_sapass
			dim darr,errinfo : errinfo=""
			form_enMode = strSafe(Request.Form("form_enMode"))
			form_uiMode = strSafe(Request.Form("form_uiMode"))
			form_dbname = strSafe(Request.Form("form_dbname"))
			form_sapass = strSafe(Request.Form("form_sapass"))
			form_sqldb  = strSafe(Request.Form("form_sqldb"))
			form_sqluid = strSafe(Request.Form("form_sqluid"))
			form_sqlpwd = strSafe(Request.Form("form_sqlpwd"))
			if not isnumeric(form_enMode) then form_enMode=0
			if not checkchar(form_sqluid) then
				errinfo=errinfo & "要生成的SQL数据库登陆名称含不合法字符\n"
			end if
			if not checkchar(form_sqldb) then
				errinfo=errinfo & "要生成的SQL数据库名称含不合法字符\n"
			end if
			if errinfo<>"" then GetAlert errinfo
			if form_sqldb="" and form_accdb<>"" then
				darr=split(form_accdb,"\")
				form_sqldb=split(darr(ubound(darr)),".")(0)
			end if
			Call SetData(form_enMode, form_uiMode, form_dbname, form_sqldb, form_sqluid, form_sqlpwd, form_sapass)
			Call openDB(temp_accdb)
			enMode=clng(enMode)
			Call CreateSQL(temp_accdb,enMode)
		Else
			if temp_accdb="" then temp_accdb="/data/mydb.mdb"
			Call Main()
		End If
	End Sub

	Sub Main()
		Response.Write tpl_header & tpl_main & tpl_footer
	End Sub

	Function SetData(Byval Mode, Byval UniMode, Byval db, Byval sqldb, Byval sqluid, Byval sqlpwd, Byval sapass)
		If CLng(Mode)>=0 Then enMode = Mode
		If CLng(Mode)>=0 Then uiMode = UniMode
		If Trim(db)<>"" Then temp_accdb = db
		If Trim(sqldb)<>"" Then temp_sqldb = sqldb
		temp_sqluid = Trim(sqluid)
		temp_sqlpwd = sqlpwd
		temp_sapass = sapass
	End Function

	Sub InitTpl()
		tpl_header = tpl_header & "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">" & VbCrlf
		tpl_header = tpl_header & "<HTML>" & VbCrlf
		tpl_header = tpl_header & "<HEAD>" & VbCrlf
		tpl_header = tpl_header & "<META http-equiv='Content-Type' content='text/html;charset=utf-8'>" & VbCrlf
		tpl_header = tpl_header & "<TITLE>Access to SQLserver 数据库生迁脚本编写器</TITLE>" & VbCrlf
		tpl_header = tpl_header & "<style>" & VbCrlf
		tpl_header = tpl_header & "table{color:#000000;font-size: 9pt;FONT-FAMILY: 'Tahoma','MS Shell Dlg';}" & VbCrlf
		tpl_header = tpl_header & "td{color: #000000;font-size: 9pt;}table{color: #000000;font-size: 9pt;FONT-FAMILY: 'Tahoma','MS Shell Dlg';}" & VbCrlf
		tpl_header = tpl_header & "body{color: #000000;font-size: 9pt;}" & VbCrlf
		tpl_header = tpl_header & "</style>" & VbCrlf
		tpl_header = tpl_header & "</HEAD>" & VbCrlf
		tpl_header = tpl_header & "<body bgCOLOR=eeeeee text='#000000' leftmargin='0' marginwidth='100%' topmargin='0' bottommargin='20'>" & VbCrlf
		'---
		tpl_main = tpl_main & "" & VbCrlf
		tpl_main = tpl_main & "<style>" & VbCrlf
		tpl_main = tpl_main & ".titlebar{FONT-WEIGHT: bold; FONT-SIZE: 12pt; FILTER: dropshadow(color=#333333, offx=1, offy=2); WIDTH: 100%; COLOR: #ffffff; FONT-FAMILY: Tahoma,Verdana, Arial, sans-serif; POSITION: relative; TOP: 1px}" & VbCrlf
		tpl_main = tpl_main & "</style>" & VbCrlf
		tpl_main = tpl_main & "<FORM METHOD=POST ACTION='?action=1' Name=DBform>" & VbCrlf
		tpl_main = tpl_main & "<TABLE width='100%' cellspacing=0 border=0>" & VbCrlf
		tpl_main = tpl_main & "	<TR bgcolor=#D4D0C8>" & VbCrlf
		tpl_main = tpl_main & "		<TD align=center height=32></td><td><span class=titlebar><font color=#ffffff><b>MiniAccess Editor V1.0 P3 (Access To SQLserver 数据升迁 脚本编写器)</b></font></span></TD>" & VbCrlf
		tpl_main = tpl_main & "	<td></td></TR>" & VbCrlf
		tpl_main = tpl_main & "<TABLE align=center width='100%' cellspacing=1 cellpadding=3 border=0>" & VbCrlf
		tpl_main = tpl_main & "</TABLE>" & VbCrlf
		tpl_main = tpl_main & "<TABLE align=center width='100%' cellspacing=1 cellpadding=3 border=0>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#667766><TD align=right  height=10></TD><TD></TD></TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0C8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right><span id=a>编写模式</span></TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD>" & VbCrlf
		tpl_main = tpl_main & "	<INPUT TYPE='radio' NAME='form_enMode' value='0' checked>Sql文本" & VbCrlf
		tpl_main = tpl_main & "	<INPUT TYPE='radio' NAME='form_enMode' value='1'>Asp代码" & VbCrlf
		tpl_main = tpl_main & "	<!-- <INPUT TYPE='radio' NAME='form_enMode' value='2'>编写完后直接运行 -->" & VbCrlf
		tpl_main = tpl_main & "	&nbsp;&nbsp;<INPUT TYPE='checkbox' NAME='form_uiMode' value='1' checked> 文本和备注按Unicode导入" & VbCrlf
		tpl_main = tpl_main & "	</TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0C8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right width=250>MDB数据库路径</TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD><INPUT TYPE='text' NAME='form_dbname' value='" & cfg_accdb & "' style='width:70%;'> </TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0C8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right width=250>SQLserver登陆帐号(sa)</TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD><INPUT TYPE='password' NAME='form_sapass' value='" & cfg_sqluid & "' style='width:30%;'></TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0D8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right width=250>导入SQL的后的数据库名</TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD><INPUT TYPE='text' NAME='form_sqldb' value='" & temp_sqldb & "' style='width:30%;'> </TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0D8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right width=250>导入SQL的数据库登陆帐号</TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD><INPUT TYPE='text' NAME='form_sqluid' value='" & temp_sqluid & "' style='width:30%;'> </TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR bgcolor=#D4D0D8>" & VbCrlf
		tpl_main = tpl_main & "	<TD align=right width=250>导入SQL的数据库登陆密码</TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD><INPUT TYPE='password' NAME='form_sqlpwd' value='" & temp_sqlpwd & "' style='width:30%;'> </TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR  bgcolor=#667766><TD align=right  height=10></TD><TD></TD></TR>" & VbCrlf
		tpl_main = tpl_main & "<TR>" & VbCrlf
		tpl_main = tpl_main & "	<TD height=38></TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD bgcolor=#D4D0C8> &nbsp; &nbsp;<INPUT TYPE='submit' value=' 确 定 ' style='width:80;'></TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "<TR>" & VbCrlf
		tpl_main = tpl_main & "	<TD height=38></TD>" & VbCrlf
		tpl_main = tpl_main & "	<TD bgcolor=#D4D0C8> &nbsp; &nbsp;" & VbCrlf
		tpl_main = tpl_main & "	<li><<简介>>" & VbCrlf
		tpl_main = tpl_main & "	<li>For Access 数据库导入 SQLserver 的版本，生成的在SQL2000下执行的 SQL脚本，<br> &nbsp; &nbsp; &nbsp; &nbsp;除了还原库结构，还同时将Access的数据导入 SQLserver" & VbCrlf
		tpl_main = tpl_main & "		<br> &nbsp; &nbsp; &nbsp; &nbsp;由于SQLserver的视图不一样，Access能自动处理同名列，<br> &nbsp; &nbsp; &nbsp; &nbsp;脚本生成对含Select *有同名列的联合查询作了自动转换，有可能需要对照重修改一下" & VbCrlf
		tpl_main = tpl_main & "	<li>功能:可编写Access数据库的常用的主要对象,包括 <br> &nbsp; &nbsp; &nbsp; &nbsp;<b>表,视图,索引,约束,包括 默认值,主键,自动编号,外键</b>(表关系)" & VbCrlf
		tpl_main = tpl_main & "	<li>编写完自动保存为原数据库名+相应扩展的文件" & VbCrlf
		tpl_main = tpl_main & "	<li>Asp模式可直接生成带表单输入的可执行的Asp文件,用生成的Asp文件即可生成新的数据库" & VbCrlf
		tpl_main = tpl_main & "	<li>Sql模式可直接生成纯Sql语句文本</li><br><br></TD>" & VbCrlf
		tpl_main = tpl_main & "</TR>" & VbCrlf
		tpl_main = tpl_main & "</Table>" & VbCrlf
		tpl_main = tpl_main & "</FORM>" & VbCrlf
		'---
		tpl_footer = tpl_footer & "<hr size=1><center>Copyright &copy; 2012</center><hr size=1><br></BODY></HTML>" & VbCrlf
	End Sub

	Sub CreateSQL(Byval dbName,Byval exec)
		'创建模式
		'exec = 0 : 生成SQL语句
		'exec = 1 : 生成Asp程序
		Response.Write tpl_header & "" & VBCrlf
		dim tbls,tabsArr,ub,I,temp,tplHead,remchar
		dim TableStr
		if exec=1 then
			tplHead="<" & "%" &"@LANGUAGE=""VBSCRIPT""%" & ">" & vbcrlf
			tplHead=tplHead & "<" & "%" &"Option Explicit" & vbcrlf
			tplHead=tplHead & "Response.Buffer=True" & vbcrlf & vbcrlf
			tplHead=tplHead & "" & vbcrlf & "'Access 数据库 SQL 脚本生成" & vbcrlf & "'=========================================================================" & vbcrlf & vbcrlf
		end if
		Dim dbPath:dbPath=dbName
		if instr(dbName,":\")=0 and instr(dbName,":/")=0 then
			dbPath=Server.MapPath(dbName)
		end if
		ConnStr="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & dbPath
		Set Conn = Server.CreateObject("ADODB.Connection")
		Conn.Open ConnStr
		'编写Conn对象
		if exec=1 then
			temp=temp & "" &"Sub CreateDB(dbName,NewDBName,loginName,loginPassword,sapass,DTS)" & vbcrlf
			temp=temp & "DIM Conn,ConnStr" & vbcrlf
			'temp=temp & "ConnStr=""Provider=Microsoft.Jet.OLEDB.4.0;Data Source="" & dbPath" & vbcrlf
			temp=temp & "ConnStr=""Provider=SQLOLEDB.1;Password='"" & sapass & ""';Persist Security InFso=true;User ID='sa';Initial Catalog='Master';Data Source='(local)';Connect Timeout=30""" & vbcrlf
			temp=temp & "Set Conn=Server.CreateObject(""ADODB.Connection"")" & vbcrlf
			temp=temp & "Conn.open ConnStr" & vbcrlf & vbcrlf
			temp=temp & "Conn.execute(""Create Database ["" & NewDBName & ""]"")" & vbcrlf
			temp=temp & "Conn.close" & vbcrlf
			temp=temp & "ConnStr=""Provider=SQLOLEDB.1;Password='"" & sapass & ""';Persist Security InFso=true;User ID='sa';Initial Catalog='"" & NewDBName & ""';Data Source='(local)';Connect Timeout=30""" & vbcrlf
			temp=temp & "Conn.open ConnStr" & vbcrlf & vbcrlf
			temp=temp & "Conn.execute(""exec sp_addlogin '"" & loginName & ""','"" & loginPassword & ""','"" & NewDBName & ""'"")" & vbcrlf
			temp=temp & "Conn.execute(""exec sp_adduser '"" & loginName & ""','"" & loginName & ""','db_owner'"")" & vbcrlf
		elseif exec=0 then
			temp=temp & "Create Database [" & temp_sqldb & "]" & vbcrlf & " go" & vbcrlf
			temp=temp & "use [" & temp_sqldb & "]" & vbcrlf & " go" & vbcrlf & vbcrlf
			temp=temp & "exec sp_addlogin '" & temp_sqluid & "','" & temp_sqlpwd & "','" & temp_sqldb & "'" & vbcrlf & " go" & vbcrlf
			temp=temp & "exec sp_adduser '" & temp_sqluid & "','" & temp_sqluid & "','db_owner'" & vbcrlf & " go" & vbcrlf
		end if
		'编写表/索引对象
		Set tbls=Conn.openSchema(20) 'adSchemaPrimaryKeys
		tbls.Filter =" TABLE_TYPE='TABLE' " '筛选出有默认值，但允许null的列
		while Not tbls.eof
			TableStr=TableStr & "|" & tbls("TABLE_Name")
			tbls.movenext
		wend
		tbls.filter=0
		tbls.close
		set tbls=nothing
		TableStr=mid(TableStr,2)
		if exec=1 then
			remchar="'"
		elseif exec=0 then
			remchar="--"
		end if
		if TableStr<>"" then
			tabsArr=split(TableStr,"|")
			ub=ubound(tabsArr)
			for I=0 to ub
				temp=temp & remchar & "[" & tabsArr(I) & "]:" &  vbcrlf
				temp=temp & CreatTableSql(tabsArr(I),exec) & vbcrlf & vbcrlf
			next
		end if
		'编写数据导入
		if exec=1 then temp=temp &  "If DTS=1 then " &  vbcrlf
		temp=temp & CreateOpenDataSource(TableStr,dbPath,exec)
		if exec=1 then temp=temp &  "End iF " &  vbcrlf
		'编写表关系
		if TableStr<>"" then temp=temp & CreatForeignSql(exec)
		'编写视图
		temp=temp & CreatViewSql(exec) & vbcrlf
		if exec=1 then
			temp=replace(temp,">",""" & chr(62) & """)
			temp=replace(temp,"<",""" & chr(60) & """)
			temp=temp & "End Sub" &  vbcrlf & vbcrlf
			temp=temp & Add_aspExec()
			temp=tplHead & temp & vbcrlf & "%" & ">"
		elseif exec=0 then
			temp=temp & "" & vbcrlf & "'---Access To SQL 数据库升迁脚本" & vbcrlf & "--=========================================================================" & vbcrlf & vbcrlf
			temp = temp & vbCrLf & "--连接字串:ConnStr=""Provider=SQLOLEDB.1;Persist Security InFso=true;Data Source='(local)';Initial Catalog='" & temp_sqldb & "';User ID='" & temp_sqluid & "';Password='" & temp_sqlpwd & "';Connect Timeout=30""" & vbCrLf & vbCrLf
		end if
		Dim ExtName:if enMode=0 then:ExtName=".Sql":else:ExtName=".Asp":end if
		call Ados_Write(temp,dbPath & ExtName,"utf-8")
		rw "<br><img width=100 height=0>" & dbPath & "的SQL脚本编写完成",1
		rw "<img width=100 height=0>已经保存文件为<b><font color=blue>" & dbPath & ExtName & "</font></b>[<a href=?>返回</a>]:",1
		rw "<center><textarea style=""width:70%;height:500px;"" wrap=""off"">" & server.Htmlencode(temp) & "</textarea></center>",1
		Response.Write tpl_footer & "" & VBCrlf
	End Sub

	Function Add_aspExec()
		dim S
		S = S & "call CreateSQLDB()" & vbCrlf
		S = S & vbCrlf
		S = S & "Sub Main()" & vbCrlf
		S = S & "	Response.write(""<html><head></head><body topmargin=0><br><center><FORM METHOD=POST><table border=1><tr><td><table cellspacing=0 cellpadding=2 align=center border=0 width=""""600"""" style=""""font-size:9pt"""" bgcolor=#D4D0C8>"")" & vbCrlf
		S = S & "	Response.write(""<tr bgcolor=#A4D0F8><td colspan=2 align=center style=""""font-size:9pt;color:#000000"""" height=30><b>Access To SQL server 导入</b>(CreateSQL脚本编写器创建)</td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr bgcolor=#667766><td colspan=2 height=1></td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right width=""""30%"""">Sa登陆密码:</td><td><input type='password' name='form_sapass' Value='" & sapass & "' style=""""width:70%;"""">(必须输入才能键库)</td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr bgcolor=#667766><td colspan=2 height=1></td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right width=""""30%"""">要导入的Access数据库:</td><td><input name='form_dbname' Value='" & temp_accdb & "' style=""""width:70%;""""></td></tr>"")" & vbCrlf
		S = S & "	" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right width=""""30%"""">新建SQL数据库名:</td><td><input name='form_sqldb' Value='" & temp_sqldb & "' style=""""width:70%;""""></td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right>新建SQL数据库登陆名:</td><td><input name='form_sqluid' Value='" & temp_sqluid & "' style=""""width:70%;""""></td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right>新建SQL数据库登陆密码:</td><td><input type='password' name='form_sqlpwd' Value='" & temp_sqlpwd & "' style=""""width:70%;""""></td></tr>"")" & vbCrlf
		S = S & "	" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right>是否导入MDB数据到SQL</td><td><input name=DTS type=radio Value='1' checked>是 <input name=DTS type=radio Value='0'>否  </td></tr>"")" & vbCrlf
		S = S & "	Response.write(""<tr><td align=right></td><td><br><INPUT TYPE=submit name=CreateDB Value="""" 确 定 """"><br><br>注:如果有外键则只建库结构再导入数据可能会出错,要导入的数据库必须和原来的编写SQL脚本的数据库结构一致</td></tr>"")" & vbCrlf
		S = S & "	Response.write(""</table></td></tr></table></FORM></center><body></html>"")" & vbCrlf
		S = S & "End Sub" & vbCrlf
		S = S & vbCrlf
		S = S & "Sub CreateSQLDB()" & vbCrlf
		S = S & "	dim NewDB_Name,loginName,loginpassword,sapass,DB_Name,DTS,Tstr" & vbCrlf
		S = S & "	NewDB_Name=strSafe(Request.Form(""form_sqldb""))" & vbCrlf
		S = S & "	loginName=strSafe(Request.Form(""form_sqluid""))" & vbCrlf
		S = S & "	loginpassword=strSafe(Request.Form(""form_sqlpwd""))" & vbCrlf
		S = S & "	sapass=strSafe(Request.Form(""sapass""))" & vbCrlf
		S = S & "	DB_Name=strSafe(Request.Form(""form_dbname""))" & vbCrlf
		S = S & "	DTS=strSafe(Request.Form(""DTS""))" & vbCrlf
		S = S & "	if isNumeric(DTS) then " & vbCrlf
		S = S & "		DTS=clng(DTS)" & vbCrlf
		S = S & "	else DTS=0" & vbCrlf
		S = S & "	end if" & vbCrlf
		S = S & "	if DTS=0 then " & vbCrlf
		S = S & "		Tstr=""创建完成"" " & vbCrlf
		S = S & "	else Tstr=""创建完成,数据已经导入""" & vbCrlf
		S = S & "	end if" & vbCrlf
		S = S & "	if NewDB_Name<>"""" then" & vbCrlf
		S = S & "		Call CreateDB(DB_Name,NewDB_Name,loginName,loginpassword,sapass,DTS)" & vbCrlf
		S = S & "		response.write vbcrlf & Tstr & ""<br>连接字串:<br>ConnStr=""""Provider=SQLOLEDB.1;Persist Security InFso=true;Data Source='(local)';Initial Catalog='"" & NewDB_Name & ""';User ID='"" & loginName & ""';Password='"" & loginpassword & ""';Connect Timeout=30""""<br>"" & vbcrlf" & vbCrlf
		S = S & "	else" & vbCrlf
		S = S & "		call main()" & vbCrlf
		S = S & "	end if" & vbCrlf
		S = S & "End Sub" & vbCrlf
		S = S & vbCrlf
		S = S & "Function strSafe(Byval s)" & vbCrlf
		S = S & "	Dim str : str = s" & vbCrlf
		S = S & "	str=replace(str,""'"","""")" & vbCrlf
		S = S & "	str=Replace(str,Chr(0),"""")" & vbCrlf
		S = S & "	str=Replace(str,"" "","""")" & vbCrlf
		S = S & "	strSafe=str" & vbCrlf
		S = S & "End Function" & vbCrlf
		S = S & vbCrlf
		Add_aspExec=S
	End Function

	Function CreateOpenDataSource(Byval TableStr,Byval dbName,exec)
		'SET IDENTITY_INSERT Co_admin ON
		'go
		'INSERT INTO dbo.Co_admin (id,username,password,MasterFlag,adduser)
		'SELECT id,username,password,MasterFlag,adduser
		'FROM OPENDATASOURCE('Microsoft.Jet.OLEDB.4.0','Data Source="d:\www\lfgbox\coosel2.0\data\coosel.asa"')...[Co_admin]
		'go
		'SET IDENTITY_INSERT dbo.Co_admin OFF
		'go
		dim splitchar,splitchar1,columnStr,rs,i,TmpStr1,tmp,remchar
		if exec=1 then
			remchar="'"
			splitchar=""""
			splitchar1=""" & _"
		elseif exec=0 then
			remchar="--"
			splitchar=""
			splitchar1=""
		end if
		Set rs=Conn.openSchema(20)
		rs.Filter ="TABLE_TYPE='TABLE'"
		while not rs.EOF
			columnStr=GetColumnStr(rs("TABLE_NAME"))
			if columnStr<>"" then
				'if n>0 then tmpStr1=tmpStr1 &  splitchar1 & vbcrlf
				TmpStr1=TmpStr1 & remchar & "[" & rs("TABLE_NAME") & "]:" &  vbcrlf
				'TmpStr1=TmpStr1 & "Conn.CommandTimeout = 600 " &  vbcrlf
				if GetAutoincrementCoulmnT(rs("TABLE_NAME"))<>"" then
					tmp="SET IDENTITY_INSERT [dbo].[" & rs("TABLE_NAME") & "] ON"
					if exec=0 then
						tmp=tmp & vbcrlf & " go " &  vbcrlf
					elseif exec=1 then
						tmp="Conn.execute(""" & tmp & """)" & vbcrlf
					end if
					TmpStr1=TmpStr1 & tmp & vbcrlf
				end if
				tmp="INSERT INTO [dbo].[" & rs("TABLE_NAME") & "] (" & columnStr & ") " &  splitchar1 & vbcrlf
				tmp=tmp & "	" & splitchar & "SELECT " & columnStr & " " &  splitchar1 & vbcrlf
				if exec=0 then
					tmp=tmp & "	" & splitchar & "FROM OPENDATASOURCE('Microsoft.Jet.OLEDB.4.0','Data Source=" & splitchar & """" & dbName & """" & splitchar & "')...[" & rs("TABLE_NAME") & "]"
					tmp=tmp & vbcrlf & " go " &  vbcrlf
				elseif  exec=1 then
					tmp=tmp & "	" & splitchar & "FROM OPENDATASOURCE('Microsoft.Jet.OLEDB.4.0','Data Source=" & splitchar & """"" & dbName & """"" & splitchar & "')...[" & rs("TABLE_NAME") & "]"
					tmp="Conn.execute(""" & tmp & """)" & vbcrlf
				end if
				TmpStr1=TmpStr1 & tmp & vbcrlf
				if GetAutoincrementCoulmnT(rs("TABLE_NAME"))<>"" then
					tmp="SET IDENTITY_INSERT [dbo].[" & rs("TABLE_NAME") & "] Off"
					if exec=0 then
						tmp=tmp & vbcrlf & " go " &  vbcrlf & vbcrlf
					elseif exec=1 then
						tmp="Conn.execute(""" & tmp & """)" & vbcrlf & vbcrlf
					end if
					TmpStr1=TmpStr1 & tmp & vbcrlf
				end if
			end if
			RS.MoveNext
		wend
		'TmpStr1=TmpStr1 & "Conn.CommandTimeout = 30 " &  vbcrlf
		rs.filter=0
		rs.close
		set rs=nothing
		CreateOpenDataSource=TmpStr1
	End Function

	Sub CreateMDB(Byval db)
		'改配置表名和列名
		dim cat,newdb
		newdb=db
		if newdb<>"" then
			if instr(newdb,":\")=0 and instr(newdb,":/")=0 then
				newdb=Server.MapPath(newdb)
			end if
			set cat=Server.CreateObject("ADOX.Catalog")
			cat.Create "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & newdb
			set cat=nothing
			CreateDB(newdb)
			response.write vbcrlf & "OK"
		else
			set cat=nothing
			call main()
		end if
	End Sub

	Function GetviewColumnStr(Byval viewName)
		dim rs,i,tmpstr,arr,j,chg
		chg=false
		set rs=server.createobject("adodb.recordset")
		rs.open "[" & viewName & "]",conn
		dim tmp
		if rs.fields.count>0 then
			tmpstr=rs(0).name
			for i=1 to rs.fields.count-1
				tmpstr=tmpstr & "," & rs(i).name
			next
			tmpstr=lcase(tmpstr)
			arr=split(tmpstr,",")
			for i=0 to ubound(arr)
				tmp=arr(i)
				arr(i)="[" & arr(i) & "]"
				if instr(arr(i),".")>0 then
					arr(i)=replace(arr(i),".","].[")
					arr(i)=arr(i) & " as " & replace(tmp,".","_")
					chg=true
				end if
			next
			if chg then
				GetviewColumnStr=join(arr,",")
			else
				GetviewColumnStr="*"
			end if
		else
			GetviewColumnStr=""
		end if
	End Function

	Function GetColumnStr(Byval tablename)
		dim rs,i,tmpstr
		set rs=server.createobject("adodb.recordset")
		rs.open "select * from [" & tablename & "] where 1=0",conn
		if rs.fields.count>0 then
			for i=0 to rs.fields.count-1
				if rs(i).type<>205 then tmpstr=tmpstr & "," & rs(i).name
			next
			if tmpstr<>"" then
				 GetColumnStr=mid(tmpstr,2)
			else GetColumnStr=""
			end if
		else
			GetColumnStr=""
		end if
	End Function

	Sub Ac2SQLStr()
		dim rs, temp : temp=""
		Set rs=Conn.openSchema(20)
		rs.Filter ="TABLE_TYPE='TABLE'"
		while not rs.EOF
			temp=temp & "SELECT  * INTO [tmp_" & rs("TABLE_NAME") & "] FROM OPENDATASOURCE('Microsoft.Jet.OLEDB.4.0','Data Source=""d:\www\lfgbox\paintblue2.0f2\pbbs\database\paintbase#.asa""')...[" & rs("TABLE_NAME") & "]<br>"
			rs.MoveNext
		wend
		rs.filter=0
		rs.close
		set rs=nothing
	End Sub

	Function CreatViewSql(Byval exec)
		dim cols
		dim FKtable,PK_cols,FK_cols,tmpStr,tmpStr1,VIEW_DEFINITION
		Set cols=Conn.openSchema(23)
		cols.filter=0
		while not cols.eof
			tmpStr1=""
			VIEW_DEFINITION=replace(cols("VIEW_DEFINITION"),chr(13),"")
			VIEW_DEFINITION=replace(VIEW_DEFINITION,chr(10)," ")
			VIEW_DEFINITION=left(VIEW_DEFINITION,len(VIEW_DEFINITION)-1)
			VIEW_DEFINITION=TransView(cols("TABLE_NAME"),VIEW_DEFINITION)
			tmpStr1="Create view [dbo].[" & cols("TABLE_NAME") & "] As " & VIEW_DEFINITION & ""
			if exec=1 then tmpStr1="Conn.execute(""" & tmpStr1 & """)"
			tmpStr=tmpStr & vbcrlf & tmpStr1
			if exec=0 then tmpStr=tmpStr & vbcrlf & " go"
			cols.movenext
		wend
		cols.close
		set cols=nothing
		CreatViewSql=tmpStr
	End Function

	Function TransView(Byval viewName,Byval s)
		dim str:str=lcase(s)
		str=replace(str,chr(9)," ")
		str=replace(str,chr(32)," ")
		str=replace(str,chr(10)," ")
		str=replace(str,chr(13),"")
		str=replace(str,";"," ")
		do while instr(str,"  ")>0
			str=replace(str,"  "," ")
		loop
		str=replace(str,"count(*)","count(*) as count_x")
		if instr(lcase(str),"* from")=0 then
			TransView=str
		else
			TransView=replace(str,"* from",GetviewColumnStr(viewName) & " from")
		end if
	End Function

	'---

	Function CreatTableSql(Byval tableName,Byval exec)
		dim cols
		dim TmpStr,TmpStr1
		Set cols=Conn.openSchema(4)
		dim splitchar,splitchar1
		if exec=1 then
			splitchar=""""
			splitchar1=""" & _"
		elseif exec=0 then
			splitchar=""
			splitchar1=""
		end if
		cols.filter="Table_name='" & tableName & "'"
		if cols.eof then
		   exit function
		end if
		dim cat,autoclumn,n,chkPrimaryKey : n=0
		'编写表脚本
		autoclumn=GetAutoincrementCoulmnT(tableName)
		tmpStr1="CREATE TABLE [dbo].[" & tableName & "] (" &  splitchar1 & vbcrlf
		dim autoclumnStr,columnStr
		if autoclumn<>"" then
			autoclumnStr="	" & splitchar & "[" &  autoclumn  & "] integer IDENTITY (1," & GetIncrement(tableName,autoclumn) & ") not null"
		end if
		do
			n=n+1
			cols.filter="Table_name='" & tableName & "' and ORDINAL_POSITION=" & n
			if cols.eof  then exit do
			if n>1 then tmpStr1=tmpStr1 & "," & splitchar1 & vbcrlf
			if autoclumn=cols("Column_name") then
				tmpStr1=tmpStr1 & autoclumnStr
			else
				tmpStr1=tmpStr1 & "	" & splitchar & "[" &  cols("Column_name")  & "] " &  lcase(datatypeStr(cols("DATA_TYPE"),cols("CHARACTER_MAXIMUM_LENGTH"))) &  defaultStr(cols("DATA_TYPE"),cols("COLUMN_DEFAULT"),exec) & nullStr(cols("IS_NULLABLE"), tablename, cols("Column_name"))
			end if
			cols.movenext
		loop
		tmpStr1=tmpStr1 & splitchar1 & vbcrlf  & "	" & splitchar & ") ON [Primary]"
		cols.close
		if exec=0 then tmpStr1=tmpStr1 & splitchar1 & vbcrlf  & "" & splitchar & " go"
		if exec=1 then
			TmpStr1="Conn.execute(""" & TmpStr1 & """)"
		end if
		tmpStr=tmpStr & vbcrlf & tmpStr1
		'编写索引脚本
		dim InxArr,i,kstr,j
		InxArr=split(getInxArr(tableName),",")
		Set cols=Conn.openSchema(12)
		for i=0 to ubound(InxArr)
			cols.filter="Table_name='" & tableName & "' and index_name='" & InxArr(i) & "'"
			kstr=""
			tmpStr1=""
			if Not isForeignIndex(tableName,InxArr(i)) then '外键索引不进行编写
				while not cols.eof
					kstr=kstr & ",[" & cols("column_name") & "] " & GetInxDesc(TableName,InxArr(i),cols("column_name"))
					cols.movenext
				wend
				if isPrimaryKey(TableName,InxArr(i)) then
					tmpStr1=tmpStr1 & " Alter TABLE [dbo].[" & tableName & "] WITH NOCHECK ADD CONSTRAINT [PK_" & tableName & "] Primary Key Clustered (" & mid(kstr,2) & ")  ON [Primary] "
				else
					tmpStr1=tmpStr1 & "CREATE "
					if isUnique(TableName,InxArr(i)) then tmpStr1=tmpStr1 & "Unique "
					tmpStr1=tmpStr1 & "INDEX [" & InxArr(i) & "] on [dbo].[" & tableName & "](" & mid(kstr,2) & ") ON [Primary]"
				end if
				if exec=1 then tmpStr1="Conn.execute(""" & tmpStr1 & """)"
				if exec=0 then tmpStr1=tmpStr1 & vbcrlf & " go"
				tmpStr=tmpStr & vbcrlf & tmpStr1
			end if
		next
		cols.close
		cols.filter=0
		CreatTableSql=TmpStr
	End Function

	Function CreatForeignSql(Byval exec)
	dim cols
	dim FKtable,PK_cols,FK_cols,tmpStr,tmpStr1
	Set cols=Conn.openSchema(27)
	cols.filter="PK_NAME<>Null"
		while not cols.eof
			tmpStr1=""
			tmpStr1="ALTER TABLE [" & cols("FK_TABLE_NAME") & "] " & _
					"Add CONSTRAINT [" & cols("FK_NAME") & "] " & _
					"FOREIGN KEY ([" & cols("FK_COLUMN_NAME") & "]) REFERENCES " & _
					"[" & cols("PK_TABLE_NAME") & "] ([" & cols("PK_COLUMN_NAME") & "]) "
			if cols("UPDATE_RULE")="CASCADE" then	tmpStr1=tmpStr1 & "ON UPDATE CASCADE "
			if cols("DELETE_RULE")="CASCADE" then	tmpStr1=tmpStr1 & "ON DELETE CASCADE "
			if exec=1 then tmpStr1="Conn.execute(""" & tmpStr1 & """)"
			tmpStr=tmpStr & vbcrlf & tmpStr1
			if exec=0 then tmpStr=tmpStr & vbcrlf & " go"
			cols.movenext
		wend
		cols.filter=0
		cols.close
		set cols=nothing
		CreatForeignSql=tmpStr
	End Function

	'判断是否是外键索引
	Function isForeignIndex(Byval TableName,Byval indexName)
		dim cols
		Set cols=Conn.openSchema(27)
		cols.filter="FK_TABLE_Name='" & TableName & "' and FK_NAME='" & indexName & "'"
		if Not cols.eof then
			isForeignIndex=true
		else
			isForeignIndex=false
		end if
	End Function
	'取得索引列的排序属性
	Function GetInxDesc(Byval TableName,Byval indexName,Byval ColumnName)
		dim cat
		set cat=Server.CreateObject("ADOX.Catalog")
		cat.ActiveConnection =ConnStr
		if cat.Tables("" & TableName & "").Indexes("" & indexName & "").Columns("" & ColumnName & "").SortOrder=2 then
			GetInxDesc="Desc"
		else
			GetInxDesc=""
		end if
		set cat=nothing
	End Function
	'取得列数组
	Function getColumArr(Byval tableName)
		dim cols,arr(),n
		redim arr(-1)
		n=0
		redim arr(n)
		set cols=Conn.openSchema(4)
		cols.filter="Table_Name='" & tableName & "'"
		while not cols.eof
			redim Preserve arr(n)
			arr(n)=cols("column_name")
			cols.movenext
			n=n+1
		wend
		cols.filter=0
		cols.close
		set cols=nothing
		getColumArr=arr
	End Function
	'取得索引数组
	Function getInxArr1(Byval tableName)
		dim cols,arr(),n,tmpCol
		redim arr(-1)
		n=0
		set cols=Conn.openSchema(12)
		cols.filter="Table_Name='" & tableName & "'"
		while not cols.eof
			if cols("index_name")<>tmpCol then
				redim Preserve arr(n)
				arr(n)=cols("index_name")
				n=n+1
			end if
			tmpCol=cols("index_name")
			cols.movenext
		wend
		cols.filter=0
		cols.close
		set cols=nothing
		getInxArr=arr
	End Function
	'取得索引数组
	Function getInxArr(Byval tablename)
		Dim cols,n,tmpCol,tmps : n = 0
		Set cols = Conn.openSchema(12)
		cols.Filter = "Table_Name='" & tablename & "'"
		While Not cols.EOF
			If cols("index_name") <> tmpCol Then
				tmps = tmps & "," & cols("index_name")
				n = n + 1
			End If
			tmpCol = cols("index_name")
			cols.movenext
		Wend
		cols.Filter = 0
		cols.Close
		Set cols = Nothing
		getInxArr = Mid(tmps, 2)
	End Function

	Function isUnique(Byval TableName,Byval IndexName)
		dim cols
		set cols=Conn.openSchema(12)
		cols.filter="Table_Name='" & TableName & "' and Index_Name='" & IndexName & "' and UNIQUE=True"
		if not cols.eof then
			isUnique=true
		else
			isUnique=false
		end if
		cols.filter=0
		cols.close
		set cols=nothing
	End Function

	Function isPrimaryKey(Byval TableName,Byval IndexName)
		dim cols
		set cols=Conn.openSchema(12)
		cols.filter="Table_Name='" & TableName & "' and Index_Name='" & IndexName & "' and PRIMARY_KEY=True"
		if not cols.eof then
			isPrimaryKey=true
		else
			isPrimaryKey=false
		end if
		cols.filter=0
		cols.close
		set cols=nothing
	End Function

	Function getPrimaryKey(Byval tablename, Byval columnname)
		dim cols
		Set cols=Conn.openSchema(12)
		cols.filter="Table_Name='" & tableName & "' and Column_Name='" & columnname & "' and PRIMARY_KEY=True"
		if not cols.eof then
			getPrimaryKey=cols("INDEX_NAME")
			'isPrimaryKey=true
		else
			getPrimaryKey=""
			'isPrimaryKey=false
		end if
		cols.filter=0
		cols.close
		set cols=nothing
	End Function

	Function existPrimaryKey(Byval tablename)
		dim cols
		Set cols=Conn.openSchema(12)
		cols.filter="Table_Name='" & tableName & "' and PRIMARY_KEY=True"
		if not cols.eof then
			existPrimaryKey=true
		else
			existPrimaryKey=false
		end if
		cols.filter=0
		cols.close
		set cols=nothing
	End Function

	'通用,内部属性取得自动编号，对SQLserver Access都可以
	Function GetAutoincrementCoulmnT(Byval tablename)
		dim i, rs
		Set rs=Server.CreateObject("adodb.recordSet")
		rs.open "select * from [" & tablename & "] where 1=0",Conn,0,1
		for i=0 to rs.fields.count-1
			if rs(i).Properties("isAutoIncrement")=True then
				GetAutoincrementCoulmnT=rs(i).name
				rs.close
				exit function
			end if
		next
		rs.close
	End Function

	Function datatypeStr(Byval DATA_TYPE,Byval CHARACTER_MAXIMUM_LENGTH)
		select case DATA_TYPE
		case 130
		if CHARACTER_MAXIMUM_LENGTH=0 then
			if uiMode="1" then
				datatypeStr="ntext"	'LongText
			else
				datatypeStr="text"	'LongText
			end if
		else
			if uiMode="1" then
				datatypeStr="nvarchar(" & CHARACTER_MAXIMUM_LENGTH & ")" '双字节必须使用 bvarchar 否则导入后截断
			else
				datatypeStr="varchar(" & CHARACTER_MAXIMUM_LENGTH & ")" '双字节必须使用 bvarchar 否则导入后截断
			end if
		end if
		case 17  datatypeStr="tinyint"
		case 2   datatypeStr="Smallint"
		case 3   datatypeStr="integer"
		case 4   datatypeStr="real" 'or  /同意词 float4
		case 5 	 datatypeStr="float" 'or  /同意词 float8
		case 6	 datatypeStr="money" 'or  /同意词  CURRENCY
		case 7	 datatypeStr="datetime"
		case 11  datatypeStr="bit"
		case 72  datatypeStr="UNIQUEIDENTIFIER"  'or  /同意词  GUID
		case 131 datatypeStr="DECIMAL"  'or  /同意词  DEC
		case 128 datatypeStr="BINARY"  'or  /同意词  DEC
		end select 'AUTOINCREMENT
	End Function

	Function defaultStr(Byval DATA_TYPE,Byval COLUMN_DEFAULT,Byval exec)
		if isNull(COLUMN_DEFAULT) then
			defaultStr=""
			exit function
		end if
		dim splitchar
		if exec=1 then
			splitchar=""""""
		elseif exec=0 then
			splitchar=""""
		end if
		COLUMN_DEFAULT = defaultStrfilter(COLUMN_DEFAULT)
		select case DATA_TYPE
		case 130
			COLUMN_DEFAULT=replace(COLUMN_DEFAULT,"""",splitchar)
			defaultStr=" Default ('" & COLUMN_DEFAULT & "')"
		Case 11
			If LCase(COLUMN_DEFAULT) = "true" Or LCase(COLUMN_DEFAULT) = "on" Or LCase(COLUMN_DEFAULT) = "yes" Then
				COLUMN_DEFAULT = 1
			Else: COLUMN_DEFAULT = 0
			End If
			defaultStr = " Default (" & COLUMN_DEFAULT & ")"
		case 128
			defaultStr=" Default (0x" & COLUMN_DEFAULT & ")"  'or  /同意词  DEC
		case 7
			If LCase(COLUMN_DEFAULT) = "now()" Or _
				LCase(COLUMN_DEFAULT) = "date()" Or _
				LCase(COLUMN_DEFAULT) = "time()" Then COLUMN_DEFAULT = "getdate()"
			if left(COLUMN_DEFAULT,1)="#" then COLUMN_DEFAULT=replace(COLUMN_DEFAULT,"#","'")
			defaultStr=" Default (" & COLUMN_DEFAULT & ")"  'or  /同意词  DEC
		case else
			defaultStr=" Default (" & COLUMN_DEFAULT & ")"
		end select
	End Function

	Function GetIncrement(Byval tablename, Byval columnName)
		dim cat
		set cat=Server.CreateObject("ADOX.Catalog")
		cat.ActiveConnection =ConnStr
		GetIncrement=cat.Tables("" & tablename & "").Columns("" & columnName & "").Properties("Increment")
		set cat=nothing
	End Function

	Function GetSeed(Byval tablename, Byval columnName)
		dim cat
		set cat=Server.CreateObject("ADOX.Catalog")
		cat.ActiveConnection =ConnStr
		GetSeed=cat.Tables("" & tablename & "").Columns("" & columnName & "").Properties("Seed")
		set cat=nothing
	End Function

	Function defaultStrfilter(Byval s)
		Dim str : str = s
		Do While Left(str, 1) = """"
			str = Mid(str, 2)
		Loop
		Do While Right(str, 1) = """"
			str = Left(str, Len(str) - 1)
		Loop
		Do While Left(str, 1) = "'"
			str = Mid(str, 2)
		Loop
		Do While Right(str, 1) = "'"
			str = Left(str, Len(str) - 1)
		Loop
		defaultStrfilter = str
	End Function

	Function nullStr(Byval IS_NULLABLE, Byval tablename, Byval columnName)
		If IS_NULLABLE Then
			If getPrimaryKey(tablename, columnName) = "" Then
				nullStr = " null "
			Else
			   nullStr = " not null "
			End If
		Else
			nullStr = " not null "
		End If
	End Function

	'断点调试 num=0 中断
	Sub rw(str,num)
		Dim istr:istr=str
		Dim inum:inum=num
		response.write str & "<br>"
		if inum=0 then response.end
	End Sub

	Function strSafe(Byval s)
		Dim str : str = s
		Str=replace(Str,"'","")
		Str=Replace(Str,Chr(0),"")
		Str=Replace(Str," ","")
		strSafe=Str
	End Function

	Function Ados_Read(Byval filename,Byval charset)
		dim osteam, temp, filepath : filepath = filename
		if instr(filename,":\")=0 and instr(filename,":/")=0 then
			filepath=Server.mappath(filename)
		end if
		set osteam=Server.CreateObject(AB.SteamName)
		osteam.mode=3
		osteam.type=2 'text
		osteam.charset=charset
		osteam.open
		osteam.loadFromFile filepath
		temp = osteam.ReadText()
		osteam.close
		set osteam=nothing
		Ados_Read = temp
	End Function

	Sub Ados_Write(Byval str,Byval filename, Byval charset)
		dim osteam, filepath : filepath = filename
		if instr(filename,":\")=0 and instr(filename,":/")=0 then
			filepath=Server.mappath(filename)
		end if
		set osteam=Server.CreateObject(AB.SteamName)
		osteam.mode=3
		osteam.type=2 'text
		osteam.charset=charset
		osteam.open
		osteam.setEos
		osteam.WriteText(str)
		osteam.SaveToFile filepath,2
		osteam.close
		set osteam=nothing
	End Sub

	Function CheckChar(Byval s)
		CheckChar=true
		dim str,i,j,strlen
		str = s
		dim ichar
		ichar=array("=","\","(",")","/","%",chr(32),"?"," & ","$",";",",","'",chr(34),chr(9),chr(0),"*",">","<","|",":","#")
		strlen=len(str)
		for i=0 to ubound(ichar)
			if instr(str,ichar(i))>0 then
				CheckChar=false
				exit function
			end if
		next
	End Function

	Sub GetAlert(Byval s)
		response.clear
		response.write "<script type=""text/javascript"">alert("""&s&""");history.back();</script>"
		if isObject(Conn) then closeDB
		response.end
	End Sub

	Sub CloseDB
		Conn.Close
		Set Conn=nothing
	End Sub

	Sub openDB(Byval db)
		if inStr(db,":/")=0 and inStr(db,":\")=0 then
			db=server.mappath(db)
		end if
		Set Conn = Server.CreateObject("ADODB.Connection")
		On Error Resume Next
		Conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & db
		if err.number<>0 then
			rw "数据库打开失败,错误为:" & err.description,0
			err.clear
		end if
		On Error Goto 0
	End Sub

End Class
%>